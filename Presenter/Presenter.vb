﻿Imports System.ComponentModel
Imports System.IO

Public Class Presenter

#Region " Properties "
    Private Property MainView As IMainForm
    Private Property MainModel As IPackage
#End Region

#Region " Constructor "
    Public Sub New(view As IMainForm)
        MainView = view
        MainView.Presenter = Me
        MainModel = New Package
        AddHandler MainModel.AppFileValidated, AddressOf OnFileValidationTeminated
        AddHandler MainModel.CreationPackageProgressChanged, AddressOf BgwCreate_ProgressChanged
        AddHandler MainModel.CreationPackageRunWorkerCompleted, AddressOf BgwCreate_RunWorkerCompleted
    End Sub
#End Region

#Region " Methods "
    Public Function CreationOngoing() As Boolean
        Return MainModel.CreationIsBusy
    End Function

    Public Sub SetDefaultOSTargetArch()
        MainModel.OsTargetArchitecture = "x64"
    End Sub

    Public Sub SelectedOsArchTarget(Index As Integer)
        MainView.OsArchTarget = Index
        MainModel.OsTargetArchitecture = If(Index = 0, "x64", "x32")
        ClearFieldsFromOSArchTarget()
        SelectedAppRootDirectory()
    End Sub

    Public Sub SelectedAppRootDirectory()
        Using fbd As New FolderBrowserDialog
            With fbd
                .Description = Messages.SelectRootPath
                .ShowNewFolderButton = False
                If (.ShowDialog = DialogResult.OK) Then
                    AppDirectoryBrowse(.SelectedPath)
                End If
            End With
        End Using
    End Sub

    Private Sub AppDirectoryBrowse(SelectedPath As String)
        If Helper.IsValidRootPath(New DirectoryInfo(SelectedPath).FullName) Then
            ClearFieldsFromAppRootDirectory()
            MainView.AppRootDirectoryPath = SelectedPath
            MainModel.AppRootDirectoryPath = SelectedPath
            MainView.AppFileGroupEnabled = True
            SelectedAppFile()
        Else
            Using Warn As New DlgPop(Messages.InvalidSelection, Messages.SelectedRootPathNotValid)
                Warn.ShowDialog()
            End Using
            MainView.AppFileGroupEnabled = False
        End If
    End Sub

    Private Sub ClearFieldsFromAppRootDirectory()
        MainView.AppFilePath = ""
        MainView.AppFileGroupEnabled = False
        MainView.AppFileDefaultIcon = Nothing
        MainView.AppFileCpuTarget = ""
        MainView.AppFileType = ""
        ClearMainFields()
        MainModel.ClearFieldsFromAppRootDirectory()
    End Sub

    Private Sub ClearFieldsFromOSArchTarget()
        MainView.AppRootDirectoryPath = ""
        MainView.AppFilePath = ""
        MainView.AppFileGroupEnabled = False
        MainView.AppFileDefaultIcon = Nothing
        MainView.AppFileCpuTarget = ""
        MainView.AppFileType = ""
        ClearMainFields()
        MainModel.ClearFieldsFromOSArchTarget()
    End Sub

    Private Sub ClearMainFields()
        MainView.AppFileCpuTargetVisible = False
        MainView.AppFileTypeVisible = False
        MainView.AppFileInfosName = ""
        MainView.AppFileInfosVersion = ""
        MainView.AppFileInfosManufacturer = ""
        MainView.AppFileCpuTarget = ""
        MainView.AppFileType = ""
        MainView.AppFileInfosCustomIconPath = ""
        MainView.AppFileInfosCustomIcon = Nothing
        MainView.AppFileInfosCustomIconRemoveButtonEnabled = False
        MainView.AppFileInfosGroupEnabled = False
        MainView.TargetInstallPath = ""
        MainView.TargetInstallPathGroupEnabled = False
        MainView.ShortcutGroupEnabled = False
        MainView.ShortcutIcon = Nothing
        MainView.CreateButtonEnabled = False
        MainView.PostInstallScriptGroupEnabled = False
        MainView.PostInstallScriptIcon = Nothing
        MainView.PostInstallScriptPath = ""
        MainView.PostInstallScriptIcon = Nothing
        MainView.PostDesInstallScriptGroupEnabled = False
        MainView.PostDesInstallScriptIcon = Nothing
        MainView.PostDesInstallScriptPath = ""
        MainView.PostDesInstallScriptIcon = Nothing
        MainView.ShortcutSelectedIndex = 0
    End Sub

    Private Sub ControlGroupsState(Enabled As Boolean)
        MainView.AppRootDirectoryGroupEnabled = Enabled
        MainView.OSArchTargetGroupEnabled = Enabled
        MainView.AppFileGroupEnabled = Enabled
        MainView.AppFileInfosGroupEnabled = Enabled
        MainView.TargetInstallPathGroupEnabled = Enabled
        MainView.ShortcutGroupEnabled = Enabled
        MainView.PostInstallScriptGroupEnabled = Enabled
        MainView.PostDesInstallScriptGroupEnabled = Enabled
        MainView.CreateButtonVisible = Enabled
        MainView.CreateProgressVisible = Not Enabled
    End Sub

    Public Sub SelectedAppFile()
        Using OfdFile As New OpenFileDialog
            With OfdFile
                .Filter = "Fichiers exécutables|*.exe;*.html;*.htm"
                .Title = Messages.SelectAppFile
                .InitialDirectory = MainModel.AppRootDirectoryPath
                If (.ShowDialog = DialogResult.OK) Then
                    AppFileFileBrowse(.FileName)
                End If
            End With
        End Using
    End Sub

    Private Sub AppFileFileBrowse(SelectedFilePath As String)
        Dim fInfo As New FileInfo(SelectedFilePath)
        Dim dInfo As New DirectoryInfo(MainModel.AppRootDirectoryPath)
        If fInfo.FullName.StartsWith(dInfo.FullName) Then
            ClearMainFields()
            Try
                If Helper.DeleteExistingFiles(dInfo) Then
                    MainView.AppFilePath = SelectedFilePath
                    MainModel.ReadFileInfos(SelectedFilePath)
                End If
            Catch ex As Exception
                Using Warn As New DlgPop("Erreur de lecture", "ERREUR : (AppFileFileBrowse) :" & ex.ToString)
                    Warn.ShowDialog()
                End Using
            End Try
        Else
            Using Warn As New DlgPop(Messages.InvalidSelection, Messages.SelectedAppFileNotValid)
                Warn.ShowDialog()
            End Using
        End If
    End Sub

    Private Sub OnFileValidationTeminated(sender As Object, e As ValidatedFile)
        MainModel.ApplicationInfos = e.Infos
        If e.IsValid Then
            MainView.AppFileCpuTargetVisible = Not e.Infos.IsHTML
            MainView.AppFileTypeVisible = Not e.Infos.IsHTML
            MainView.AppFileDefaultIcon = e.Infos.AppDefaultIcon
            MainView.AppFileInfosName = e.Infos.ProductName
            MainView.AppFileInfosVersion = e.Infos.FileVersion
            MainView.AppFileInfosManufacturer = e.Infos.CompanyName
            MainView.AppFileCpuTarget = e.Infos.CpuTarget
            MainView.AppFileType = e.Infos.FileType
            MainView.AppFileInfosGroupEnabled = True

            If e.Infos.IsHTML Then SelectCustomIcon()

            MainView.TargetInstallPath = e.Infos.InstallPath
        Else
            Using Warn As New DlgPop(Messages.InvalidSelection, "AVERTISSEMENT : " & e.Message)
                Warn.ShowDialog()
                MainView.AppFilePath = ""
                MainView.AppFileDefaultIcon = Nothing
            End Using
        End If
    End Sub

    Public Sub SelectCustomIcon()
        Using OfdIcon As New OpenFileDialog
            With OfdIcon
                .Filter = "Icône|*.ico"
                .Title = If(MainModel.ApplicationInfos.IsHTML, Messages.SelectCustomIconHtml, Messages.SelectCustomIcon)
                If (.ShowDialog = DialogResult.OK) Then
                    MainView.AppFileInfosCustomIconPath = .FileName
                    Dim CustomIcon = New Icon(.FileName, New Size(32, 32)).ToBitmap
                    MainView.AppFileInfosCustomIcon = CustomIcon
                    MainView.ShortcutIcon = If(MainView.ShortcutSelectedIndex = 0, Nothing, Helper.SetIconFromIconFilePath(.FileName))
                    MainView.AppFileInfosCustomIconRemoveButtonEnabled = True
                    MainModel.SetCustomIcon(.FileName, CustomIcon)
                End If
            End With
        End Using
    End Sub

    Public Sub RemoveCustomIcon()
        MainView.AppFileInfosCustomIconPath = ""
        MainView.AppFileInfosCustomIcon = Nothing
        MainView.AppFileInfosCustomIconRemoveButtonEnabled = False
        MainView.ShortcutIcon = If(MainView.ShortcutSelectedIndex = 0, Nothing, Helper.SetIconFromExeFilePath(MainModel.ApplicationInfos.AppFilePath))
        MainModel.SetCustomIcon("", Nothing)
    End Sub

    Public Sub AppFileInfosNameChanged()
        If IsValidInfosName() AndAlso IsValidVersion() AndAlso Helper.IsValidTargetInstallPath(MainView.TargetInstallPath) Then
            MainView.ShortcutGroupEnabled = True
            MainView.PostInstallScriptGroupEnabled = True
            MainView.PostDesInstallScriptGroupEnabled = True
            MainView.CreateButtonEnabled = True
            MainModel.GenerateFileInfosFromDatas(MainView.AppFileInfosName, MainView.AppFileInfosVersion, MainView.AppFileInfosManufacturer, MainView.TargetInstallPath)
        Else
            MainView.ShortcutGroupEnabled = False
            MainView.PostInstallScriptGroupEnabled = False
            MainView.PostDesInstallScriptGroupEnabled = False
            MainView.CreateButtonEnabled = False
        End If
    End Sub

    Private Function IsValidInfosName() As Boolean
        If Helper.IsInvalidPathChars(MainView.AppFileInfosName) = False Then
            MainView.TargetInstallPathGroupEnabled = True
            Return True
        Else
            MainView.TargetInstallPathGroupEnabled = False
            Return False
        End If
    End Function

    Public Sub AppFileVersionChanged()
        If IsValidVersion() AndAlso Helper.IsValidTargetInstallPath(MainView.TargetInstallPath) AndAlso IsValidInfosName() Then
            MainView.ShortcutGroupEnabled = True
            MainView.PostInstallScriptGroupEnabled = True
            MainView.PostDesInstallScriptGroupEnabled = True
            MainView.CreateButtonEnabled = True
            MainModel.GenerateFileInfosFromDatas(MainView.AppFileInfosName, MainView.AppFileInfosVersion, MainView.AppFileInfosManufacturer, MainView.TargetInstallPath)
        Else
            MainView.ShortcutGroupEnabled = False
            MainView.PostInstallScriptGroupEnabled = False
            MainView.PostDesInstallScriptGroupEnabled = False
            MainView.CreateButtonEnabled = False
        End If
    End Sub

    Private Function IsValidVersion() As Boolean
        If Version.TryParse(MainView.AppFileInfosVersion, New Version) Then
            MainView.TargetInstallPathGroupEnabled = True
            Return True
        Else
            MainView.TargetInstallPathGroupEnabled = False
            Return False
        End If
    End Function

    Public Sub ManufacturerChanged()
        MainModel.ApplicationInfos.CompanyName = MainView.AppFileInfosManufacturer
    End Sub

    Public Sub TargetInstallPathChanged()
        If Helper.IsValidTargetInstallPath(MainView.TargetInstallPath) AndAlso IsValidInfosName() AndAlso IsValidVersion() Then
            MainView.ShortcutGroupEnabled = True
            MainView.PostInstallScriptGroupEnabled = True
            MainView.PostDesInstallScriptGroupEnabled = True
            MainView.CreateButtonEnabled = True
            MainModel.GenerateFileInfosFromDatas(MainView.AppFileInfosName, MainView.AppFileInfosVersion, MainView.AppFileInfosManufacturer, MainView.TargetInstallPath)
        Else
            MainView.ShortcutGroupEnabled = False
            MainView.PostInstallScriptGroupEnabled = False
            MainView.PostDesInstallScriptGroupEnabled = False
            MainView.CreateButtonEnabled = False
        End If
    End Sub

    Public Sub SelectShortcut()
        MainModel.ShortcutIndex = MainView.ShortcutSelectedIndex
        If MainView.ShortcutSelectedIndex = 0 Then
            MainView.ShortcutIcon = Nothing
        ElseIf MainView.ShortcutSelectedIndex = 1 Then
            SetShortcutIcon()
        Else MainView.ShortcutSelectedIndex = 2
            SetShortcutIcon()
        End If
    End Sub

    Private Sub SetShortcutIcon()
        If Not File.Exists(MainView.AppFileInfosCustomIconPath) Then
            MainView.ShortcutIcon = Helper.SetIconFromExeFilePath(MainModel.ApplicationInfos.AppFilePath)
        Else
            MainView.ShortcutIcon = Helper.SetIconFromIconFilePath(MainView.AppFileInfosCustomIconPath)
        End If
    End Sub

    Public Sub RemoveInstallPostScript()
        MainView.PostInstallScriptPath = ""
        MainView.PostInstallScriptIcon = Nothing
        MainView.PostInstallRemoveButtonEnabled = False
        MainModel.SetInstallPostScript("")
    End Sub

    Public Sub SelectPostInstallScript()
        Using OfdIcon As New OpenFileDialog
            With OfdIcon
                .Filter = "Scripts|*.cmd;*.bat"
                .Title = Messages.SelectPostInstallScriptFile
                If (.ShowDialog = DialogResult.OK) Then
                    If Helper.FileContentContainsString(.FileName, "pause") Then
                        Using Warn As New DlgQuestion(Messages.SelectPostScriptPauseDetectionTitle, Messages.SelectPostInstallPauseDetectionMessage)
                            If Warn.ShowDialog() = DialogResult.Yes Then
                                PostInstallScriptValid(.FileName)
                            End If
                        End Using
                    Else
                        PostInstallScriptValid(.FileName)
                    End If
                End If
            End With
        End Using
    End Sub

    Private Sub PostInstallScriptValid(fName As String)
        MainView.PostInstallScriptPath = fName
        MainView.PostInstallScriptIcon = Icon.ExtractAssociatedIcon(fName).ToBitmap
        MainView.PostInstallRemoveButtonEnabled = True
        MainModel.SetInstallPostScript(fName)
    End Sub

    Public Sub RemoveDesInstallPostScript()
        MainView.PostDesInstallScriptPath = ""
        MainView.PostDesInstallScriptIcon = Nothing
        MainView.PostDesInstallRemoveButtonEnabled = False
        MainModel.SetDesInstallPostScript("")
    End Sub

    Public Sub SelectPostDesInstallScript()
        Using OfdIcon As New OpenFileDialog
            With OfdIcon
                .Filter = "Scripts|*.cmd;*.bat"
                .Title = Messages.SelectPostDesInstallScriptFile
                If (.ShowDialog = DialogResult.OK) Then
                    If Helper.FileContentContainsString(.FileName, "pause") Then
                        Using Warn As New DlgQuestion(Messages.SelectPostScriptPauseDetectionTitle, Messages.SelectPostDesInstallPauseDetectionMessage)
                            If Warn.ShowDialog() = DialogResult.Yes Then
                                PostDesInstallScriptValid(.FileName)
                            End If
                        End Using
                    Else
                        PostDesInstallScriptValid(.FileName)
                    End If
                End If
            End With
        End Using
    End Sub

    Private Sub PostDesInstallScriptValid(fName As String)
        MainView.PostDesInstallScriptPath = fName
        MainView.PostDesInstallScriptIcon = Icon.ExtractAssociatedIcon(fName).ToBitmap
        MainView.PostDesInstallRemoveButtonEnabled = True
        MainModel.SetDesInstallPostScript(fName)
    End Sub

    Public Sub CreateSETUP()
        If MainModel.CreationIsBusy = False Then
            ControlGroupsState(False)
            MainModel.CreatePackage()
        End If
    End Sub

    Private Sub BgwCreate_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)
        MainView.ShowCreateProgress(e.ProgressPercentage)
    End Sub

    Private Sub BgwCreate_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        If e.Result IsNot Nothing Then
            If e.Result.ToString.StartsWith("INFO") Then
                Dim ResultStr = e.Result.ToString.Split("|")
                Using dlg As New DlgEnd(ResultStr(1), ResultStr(2), ResultStr(3))
                    If dlg.ShowDialog() = DialogResult.OK Then
                        ControlGroupsState(True)
                    End If
                End Using
            Else
                Using Warn As New DlgPop(Messages.BinariesCreationEnded, e.Result.ToString)
                    Warn.ShowDialog()
                End Using
                ControlGroupsState(True)
            End If
        Else
            Using Warn As New DlgPop(Messages.BinariesCreationEnded, Messages.BinariesCreationError)
                Warn.ShowDialog()
            End Using
            ControlGroupsState(True)
        End If
    End Sub
#End Region

End Class
