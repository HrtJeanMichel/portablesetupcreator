﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DlgEnd
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.BtnTargetPath = New System.Windows.Forms.Button()
        Me.BgxDetectionFile = New System.Windows.Forms.GroupBox()
        Me.BtnDetectionFileNameCopy = New System.Windows.Forms.Button()
        Me.BtnDetectionFilePathCopy = New System.Windows.Forms.Button()
        Me.TxbDetectionFileName = New System.Windows.Forms.TextBox()
        Me.TxbDetectionFilePath = New System.Windows.Forms.TextBox()
        Me.LblDetectionFileName = New System.Windows.Forms.Label()
        Me.LblDetectionFilePath = New System.Windows.Forms.Label()
        Me.LblMessage = New System.Windows.Forms.Label()
        Me.PcbMessage = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.BgxDetectionFile.SuspendLayout()
        CType(Me.PcbMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(522, 172)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(71, 27)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(65, 21)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Fermer"
        '
        'BtnTargetPath
        '
        Me.BtnTargetPath.Location = New System.Drawing.Point(12, 58)
        Me.BtnTargetPath.Name = "BtnTargetPath"
        Me.BtnTargetPath.Size = New System.Drawing.Size(581, 30)
        Me.BtnTargetPath.TabIndex = 2
        Me.BtnTargetPath.Text = "Ouvrir l'Emplacement des binaires :"
        Me.BtnTargetPath.UseVisualStyleBackColor = True
        '
        'BgxDetectionFile
        '
        Me.BgxDetectionFile.Controls.Add(Me.BtnDetectionFileNameCopy)
        Me.BgxDetectionFile.Controls.Add(Me.BtnDetectionFilePathCopy)
        Me.BgxDetectionFile.Controls.Add(Me.TxbDetectionFileName)
        Me.BgxDetectionFile.Controls.Add(Me.TxbDetectionFilePath)
        Me.BgxDetectionFile.Controls.Add(Me.LblDetectionFileName)
        Me.BgxDetectionFile.Controls.Add(Me.LblDetectionFilePath)
        Me.BgxDetectionFile.Location = New System.Drawing.Point(12, 94)
        Me.BgxDetectionFile.Name = "BgxDetectionFile"
        Me.BgxDetectionFile.Size = New System.Drawing.Size(581, 71)
        Me.BgxDetectionFile.TabIndex = 3
        Me.BgxDetectionFile.TabStop = False
        Me.BgxDetectionFile.Text = "Méthode de détection de fichier"
        '
        'BtnDetectionFileNameCopy
        '
        Me.BtnDetectionFileNameCopy.Location = New System.Drawing.Point(522, 42)
        Me.BtnDetectionFileNameCopy.Name = "BtnDetectionFileNameCopy"
        Me.BtnDetectionFileNameCopy.Size = New System.Drawing.Size(53, 21)
        Me.BtnDetectionFileNameCopy.TabIndex = 5
        Me.BtnDetectionFileNameCopy.Text = "Copier"
        Me.BtnDetectionFileNameCopy.UseVisualStyleBackColor = True
        '
        'BtnDetectionFilePathCopy
        '
        Me.BtnDetectionFilePathCopy.Location = New System.Drawing.Point(522, 18)
        Me.BtnDetectionFilePathCopy.Name = "BtnDetectionFilePathCopy"
        Me.BtnDetectionFilePathCopy.Size = New System.Drawing.Size(53, 21)
        Me.BtnDetectionFilePathCopy.TabIndex = 4
        Me.BtnDetectionFilePathCopy.Text = "Copier"
        Me.BtnDetectionFilePathCopy.UseVisualStyleBackColor = True
        '
        'TxbDetectionFileName
        '
        Me.TxbDetectionFileName.Location = New System.Drawing.Point(60, 44)
        Me.TxbDetectionFileName.Name = "TxbDetectionFileName"
        Me.TxbDetectionFileName.ReadOnly = True
        Me.TxbDetectionFileName.Size = New System.Drawing.Size(456, 18)
        Me.TxbDetectionFileName.TabIndex = 3
        '
        'TxbDetectionFilePath
        '
        Me.TxbDetectionFilePath.Location = New System.Drawing.Point(60, 20)
        Me.TxbDetectionFilePath.Name = "TxbDetectionFilePath"
        Me.TxbDetectionFilePath.ReadOnly = True
        Me.TxbDetectionFilePath.Size = New System.Drawing.Size(456, 18)
        Me.TxbDetectionFilePath.TabIndex = 2
        '
        'LblDetectionFileName
        '
        Me.LblDetectionFileName.AutoSize = True
        Me.LblDetectionFileName.Location = New System.Drawing.Point(19, 47)
        Me.LblDetectionFileName.Name = "LblDetectionFileName"
        Me.LblDetectionFileName.Size = New System.Drawing.Size(30, 12)
        Me.LblDetectionFileName.TabIndex = 1
        Me.LblDetectionFileName.Text = "Nom :"
        '
        'LblDetectionFilePath
        '
        Me.LblDetectionFilePath.AutoSize = True
        Me.LblDetectionFilePath.Location = New System.Drawing.Point(6, 23)
        Me.LblDetectionFilePath.Name = "LblDetectionFilePath"
        Me.LblDetectionFilePath.Size = New System.Drawing.Size(42, 12)
        Me.LblDetectionFilePath.TabIndex = 0
        Me.LblDetectionFilePath.Text = "Chemin :"
        '
        'LblMessage
        '
        Me.LblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMessage.ForeColor = System.Drawing.Color.DarkGreen
        Me.LblMessage.Location = New System.Drawing.Point(83, 5)
        Me.LblMessage.Name = "LblMessage"
        Me.LblMessage.Size = New System.Drawing.Size(456, 47)
        Me.LblMessage.TabIndex = 4
        Me.LblMessage.Text = "Vous pouvez créer la règle de détection de fichier sous SCCM à l'aide des informa" &
    "tions ci-dessous"
        Me.LblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PcbMessage
        '
        Me.PcbMessage.Location = New System.Drawing.Point(12, 5)
        Me.PcbMessage.Name = "PcbMessage"
        Me.PcbMessage.Size = New System.Drawing.Size(48, 44)
        Me.PcbMessage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbMessage.TabIndex = 6
        Me.PcbMessage.TabStop = False
        '
        'DlgEnd
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(605, 210)
        Me.Controls.Add(Me.PcbMessage)
        Me.Controls.Add(Me.LblMessage)
        Me.Controls.Add(Me.BgxDetectionFile)
        Me.Controls.Add(Me.BtnTargetPath)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DlgEnd"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Fin de créaton"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.BgxDetectionFile.ResumeLayout(False)
        Me.BgxDetectionFile.PerformLayout()
        CType(Me.PcbMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents BtnTargetPath As Button
    Friend WithEvents BgxDetectionFile As GroupBox
    Friend WithEvents BtnDetectionFileNameCopy As Button
    Friend WithEvents BtnDetectionFilePathCopy As Button
    Friend WithEvents TxbDetectionFileName As TextBox
    Friend WithEvents TxbDetectionFilePath As TextBox
    Friend WithEvents LblDetectionFileName As Label
    Friend WithEvents LblDetectionFilePath As Label
    Friend WithEvents LblMessage As Label
    Friend WithEvents PcbMessage As PictureBox
End Class
