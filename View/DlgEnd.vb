﻿Imports System.Media

Public Class DlgEnd

#Region " Constructor "
    Public Sub New(Target As String, FilePath As String, FileName As String)
        InitializeComponent()
        PcbMessage.Image = Bitmap.FromHicon(SystemIcons.Information.Handle)
        BtnTargetPath.Tag = Target
        TxbDetectionFilePath.Text = FilePath
        TxbDetectionFileName.Text = FileName
    End Sub
#End Region

#Region " Methods "
    Private Sub DlgEnd_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        SystemSounds.Exclamation.Play()
    End Sub

    Private Sub OK_Button_Click(sender As Object, e As EventArgs) Handles OK_Button.Click
        Close()
    End Sub

    Private Sub BtnTargetPath_Click(sender As Object, e As EventArgs) Handles BtnTargetPath.Click
        Try
            Process.Start(BtnTargetPath.Tag.ToString)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BtnDetectionFilePathCopy_Click(sender As Object, e As EventArgs) Handles BtnDetectionFilePathCopy.Click
        Try
            Clipboard.SetText(TxbDetectionFilePath.Text)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BtnDetectionFileNameCopy_Click(sender As Object, e As EventArgs) Handles BtnDetectionFileNameCopy.Click
        Try
            Clipboard.SetText(TxbDetectionFileName.Text)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DlgEnd_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        DialogResult = DialogResult.OK
    End Sub
#End Region

End Class
