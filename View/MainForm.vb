﻿
Imports System.Runtime.InteropServices

Public Class MainForm
    Implements IMainForm

#Region " Properties "
    Public Property Presenter As Presenter Implements IMainForm.Presenter

    Public Property OsArchTarget As Integer Implements IMainForm.OsArchTarget
        Get
            Return CbxArchOS.SelectedIndex
        End Get
        Set(value As Integer)
            CbxArchOS.SelectedIndex = value
        End Set
    End Property

    Public Property AppFileDefaultIcon As Image Implements IMainForm.AppFileDefaultIcon
        Get
            Return PcbAppFileIcon.Image
        End Get
        Set(value As Image)
            PcbAppFileIcon.Image = value
        End Set
    End Property

    Public Property AppFileCpuTarget As String Implements IMainForm.AppFileCpuTarget
        Get
            Return TxbAppFileCpuTarget.Text
        End Get
        Set(value As String)
            TxbAppFileCpuTarget.Text = value
        End Set
    End Property

    Public Property AppFileType As String Implements IMainForm.AppFileType
        Get
            Return TxbAppFileType.Text
        End Get
        Set(value As String)
            TxbAppFileType.Text = value
        End Set
    End Property

    Public Property AppFileInfosName As String Implements IMainForm.AppFileInfosName
        Get
            Return TxbInfosName.Text
        End Get
        Set(value As String)
            TxbInfosName.Text = value
        End Set
    End Property

    Public Property AppFileInfosVersion As String Implements IMainForm.AppFileInfosVersion
        Get
            Return TxbInfosVersion.Text
        End Get
        Set(value As String)
            TxbInfosVersion.Text = value
        End Set
    End Property

    Public Property AppFileInfosManufacturer As String Implements IMainForm.AppFileInfosManufacturer
        Get
            Return TxbInfosManufacturer.Text
        End Get
        Set(value As String)
            TxbInfosManufacturer.Text = value
        End Set
    End Property

    Public Property AppFileInfosCustomIconPath As String Implements IMainForm.AppFileInfosCustomIconPath
        Get
            Return TxbInfosIcon.Text
        End Get
        Set(value As String)
            TxbInfosIcon.Text = value
        End Set
    End Property

    Public Property AppFileInfosCustomIcon As Image Implements IMainForm.AppFileInfosCustomIcon
        Get
            Return PcbInfosIcon.Image
        End Get
        Set(value As Image)
            PcbInfosIcon.Image = value
        End Set
    End Property

    Public Property TargetInstallPath As String Implements IMainForm.TargetInstallPath
        Get
            Return TxbTargetInstallPath.Text
        End Get
        Set(value As String)
            TxbTargetInstallPath.Text = value
        End Set
    End Property

    Public Property ShortcutSelectedIndex As Integer Implements IMainForm.ShortcutSelectedIndex
        Get
            Return CbxShortcut.SelectedIndex
        End Get
        Set(value As Integer)
            CbxShortcut.SelectedIndex = value
        End Set
    End Property

    Public Property PostInstallScriptPath As String Implements IMainForm.PostInstallScriptPath
        Get
            Return TxbPostInstall.Text
        End Get
        Set(value As String)
            TxbPostInstall.Text = value
        End Set
    End Property

    Public Property PostInstallScripticon As Image Implements IMainForm.PostInstallScriptIcon
        Get
            Return PcbPostInstallIcon.Image
        End Get
        Set(value As Image)
            PcbPostInstallIcon.Image = value
        End Set
    End Property

    Public Property PostDesInstallScriptPath As String Implements IMainForm.PostDesInstallScriptPath
        Get
            Return TxbPostDesInstall.Text
        End Get
        Set(value As String)
            TxbPostDesInstall.Text = value
        End Set
    End Property

    Public Property PostDesInstallScripticon As Image Implements IMainForm.PostDesInstallScriptIcon
        Get
            Return PcbPostDesInstallIcon.Image
        End Get
        Set(value As Image)
            PcbPostDesInstallIcon.Image = value
        End Set
    End Property

    Public Property EnabledCreateButton As Boolean Implements IMainForm.CreateButtonEnabled
        Get
            Return BtnCreate.Enabled
        End Get
        Set(value As Boolean)
            BtnCreate.Enabled = value
        End Set
    End Property
#End Region

#Region " WriteOnly Properties "
    Private WriteOnly Property EnabledOSArchTargetGroup As Boolean Implements IMainForm.OSArchTargetGroupEnabled
        Set(value As Boolean)
            GbxArchOS.Enabled = value
        End Set
    End Property
    Private WriteOnly Property EnabledAppRootDirectoryGroup As Boolean Implements IMainForm.AppRootDirectoryGroupEnabled
        Set(value As Boolean)
            GbxAppDirectory.Enabled = value
        End Set
    End Property

    Private WriteOnly Property AppRootDirectoryPath As String Implements IMainForm.AppRootDirectoryPath
        Set(value As String)
            TxbAppDirectory.Text = value
        End Set
    End Property

    Private WriteOnly Property EnabledSelectedAppFileGroup As Boolean Implements IMainForm.AppFileGroupEnabled
        Set(value As Boolean)
            GbxAppFile.Enabled = value
        End Set
    End Property

    Private WriteOnly Property AppFilePath As String Implements IMainForm.AppFilePath
        Set(value As String)
            TxbAppFilePath.Text = value
        End Set
    End Property

    Private WriteOnly Property AppFileCpuTargetVisible As Boolean Implements IMainForm.AppFileCpuTargetVisible
        Set(value As Boolean)
            LblAppFileCpuTarget.Visible = value
            TxbAppFileCpuTarget.Visible = value
        End Set
    End Property

    Private WriteOnly Property AppFileTypeVisible As Boolean Implements IMainForm.AppFileTypeVisible
        Set(value As Boolean)
            LblAppFileType.Visible = value
            TxbAppFileType.Visible = value
        End Set
    End Property

    Private WriteOnly Property EnabledAppFileInfosGroup As Boolean Implements IMainForm.AppFileInfosGroupEnabled
        Set(value As Boolean)
            GbxInfos.Enabled = value
        End Set
    End Property

    Private WriteOnly Property EnabledAppFileInfosCustomIconRemoveButton As Boolean Implements IMainForm.AppFileInfosCustomIconRemoveButtonEnabled
        Set(value As Boolean)
            BtnInfosIconRemove.Enabled = value
        End Set
    End Property

    Private WriteOnly Property EnabledTargetInstallPathGroup As Boolean Implements IMainForm.TargetInstallPathGroupEnabled
        Set(value As Boolean)
            GbxTargetInstall.Enabled = value
        End Set
    End Property

    Private WriteOnly Property ShortcutIcon As Image Implements IMainForm.ShortcutIcon
        Set(value As Image)
            PcbShortcutIcon.Image = value
        End Set
    End Property

    Private WriteOnly Property EnabledShortcutGroup As Boolean Implements IMainForm.ShortcutGroupEnabled
        Set(value As Boolean)
            GbxShortcut.Enabled = value
        End Set
    End Property

    Private WriteOnly Property EnabledPostInstallRemoveButton As Boolean Implements IMainForm.PostInstallRemoveButtonEnabled
        Set(value As Boolean)
            BtnPostInstallRemove.Enabled = value
        End Set
    End Property

    Private WriteOnly Property EnabledPostInstallScriptGroup As Boolean Implements IMainForm.PostInstallScriptGroupEnabled
        Set(value As Boolean)
            GbxPostInstall.Enabled = value
        End Set
    End Property


    Private WriteOnly Property EnabledPostDesInstallRemoveButton As Boolean Implements IMainForm.PostDesInstallRemoveButtonEnabled
        Set(value As Boolean)
            BtnPostDesInstallRemove.Enabled = value
        End Set
    End Property

    Private WriteOnly Property EnabledPostDesInstallScriptGroup As Boolean Implements IMainForm.PostDesInstallScriptGroupEnabled
        Set(value As Boolean)
            GbxPostDesInstall.Enabled = value
        End Set
    End Property

    Private WriteOnly Property CreateButtonVisible As Boolean Implements IMainForm.CreateButtonVisible
        Set(value As Boolean)
            BtnCreate.Visible = value
        End Set
    End Property

    Private WriteOnly Property CreateProgressVisible As Boolean Implements IMainForm.CreateProgressVisible
        Set(value As Boolean)
            PgbCreate.Visible = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        Text &= " " & My.Application.Info.Version.ToString
        Icon = My.Resources.SETUP
        RemoveHandler CbxArchOS.SelectedIndexChanged, AddressOf CbxArchOS_SelectedIndexChanged
    End Sub
#End Region

#Region " Methods "
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Presenter.SetDefaultOSTargetArch()
    End Sub

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Presenter.CreationOngoing Then
            e.Cancel = True
        End If
    End Sub

    Private Sub MainForm_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        CbxShortcut.SelectedIndex = 0
        CbxArchOS.SelectedIndex = 0
        AddHandler CbxArchOS.SelectedIndexChanged, AddressOf CbxArchOS_SelectedIndexChanged
    End Sub

    Private Sub CbxArchOS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxArchOS.SelectedIndexChanged
        Presenter.SelectedOsArchTarget(CbxArchOS.SelectedIndex)
    End Sub

    Private Sub BtnAppDirectory_Click(sender As Object, e As EventArgs) Handles BtnAppDirectory.Click
        Presenter.SelectedAppRootDirectory()
    End Sub

    Private Sub BtnAppFilePathBrowse_Click(sender As Object, e As EventArgs) Handles BtnAppFilePathBrowse.Click
        Presenter.SelectedAppFile()
    End Sub

    Private Sub TxbInfosName_TextChanged(sender As Object, e As EventArgs) Handles TxbInfosName.TextChanged
        Presenter.AppFileInfosNameChanged()
    End Sub

    Private Sub TxbInfosVersion_TextChanged(sender As Object, e As EventArgs) Handles TxbInfosVersion.TextChanged
        Presenter.AppFileVersionChanged()
    End Sub

    Private Sub BtnInfosIconRemove_Click(sender As Object, e As EventArgs) Handles BtnInfosIconRemove.Click
        Presenter.RemoveCustomIcon()
    End Sub

    Private Sub BtnInfosIcon_Click(sender As Object, e As EventArgs) Handles BtnInfosIcon.Click
        Presenter.SelectCustomIcon()
    End Sub

    Private Sub TxbTargetInstallPath_TextChanged(sender As Object, e As EventArgs) Handles TxbTargetInstallPath.TextChanged
        Presenter.TargetInstallPathChanged()
    End Sub

    Private Sub CbxShortcut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxShortcut.SelectedIndexChanged
        Presenter.SelectShortcut()
    End Sub

    Private Sub TxbInfosManufacturer_TextChanged(sender As Object, e As EventArgs) Handles TxbInfosManufacturer.TextChanged
        Presenter.ManufacturerChanged()
    End Sub

    Private Sub BtnPostInstallRemove_Click(sender As Object, e As EventArgs) Handles BtnPostInstallRemove.Click
        Presenter.RemoveInstallPostScript()
    End Sub

    Private Sub BtnPostInstallBrowse_Click(sender As Object, e As EventArgs) Handles BtnPostInstallBrowse.Click
        Presenter.SelectPostInstallScript()
    End Sub

    Private Sub BtnPostDesInstallRemove_Click(sender As Object, e As EventArgs) Handles BtnPostDesInstallRemove.Click
        Presenter.RemoveDesInstallPostScript()
    End Sub

    Private Sub BtnPostDesInstallBrowse_Click(sender As Object, e As EventArgs) Handles BtnPostDesInstallBrowse.Click
        Presenter.SelectPostDesInstallScript()
    End Sub

    Private Sub BtnCreate_Click(sender As Object, e As EventArgs) Handles BtnCreate.Click
        Presenter.CreateSETUP()
    End Sub

    Public Sub ShowCreateProgress(Value As Integer) Implements IMainForm.ShowCreateProgress
        If PgbCreate.InvokeRequired Then
            PgbCreate.Invoke(New MethodInvoker(Sub()
                                                   PgbCreate.Value = Value
                                               End Sub))
        Else
            PgbCreate.Value = Value
        End If
    End Sub

    Private Sub LnkLblBitbucket_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LnkLblBitbucket.LinkClicked
        Try
            Process.Start(LnkLblBitbucket.Text)
        Catch ex As Exception
        End Try
    End Sub

#End Region

End Class
