﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form
    Implements IMainForm


    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BgwCreate = New System.ComponentModel.BackgroundWorker()
        Me.LnkLblBitbucket = New System.Windows.Forms.LinkLabel()
        Me.GbxArchOS = New System.Windows.Forms.GroupBox()
        Me.CbxArchOS = New System.Windows.Forms.ComboBox()
        Me.LblArchOS = New System.Windows.Forms.Label()
        Me.GbxAppFile = New System.Windows.Forms.GroupBox()
        Me.LblAppFilePath = New System.Windows.Forms.Label()
        Me.LblAppFileType = New System.Windows.Forms.Label()
        Me.PcbAppFileIcon = New System.Windows.Forms.PictureBox()
        Me.TxbAppFileType = New System.Windows.Forms.TextBox()
        Me.TxbAppFilePath = New System.Windows.Forms.TextBox()
        Me.LblAppFileCpuTarget = New System.Windows.Forms.Label()
        Me.TxbAppFileCpuTarget = New System.Windows.Forms.TextBox()
        Me.BtnAppFilePathBrowse = New System.Windows.Forms.Button()
        Me.BtnCreate = New System.Windows.Forms.Button()
        Me.GbxTargetInstall = New System.Windows.Forms.GroupBox()
        Me.LblTargetInstall = New System.Windows.Forms.Label()
        Me.TxbTargetInstallPath = New System.Windows.Forms.TextBox()
        Me.PgbCreate = New System.Windows.Forms.ProgressBar()
        Me.GbxInfos = New System.Windows.Forms.GroupBox()
        Me.BtnInfosIconRemove = New System.Windows.Forms.Button()
        Me.LblInfosIcon = New System.Windows.Forms.Label()
        Me.TxbInfosIcon = New System.Windows.Forms.TextBox()
        Me.BtnInfosIcon = New System.Windows.Forms.Button()
        Me.LblInfosManufacturer = New System.Windows.Forms.Label()
        Me.TxbInfosManufacturer = New System.Windows.Forms.TextBox()
        Me.LblInfosVersion = New System.Windows.Forms.Label()
        Me.TxbInfosVersion = New System.Windows.Forms.TextBox()
        Me.LblInfosName = New System.Windows.Forms.Label()
        Me.PcbInfosIcon = New System.Windows.Forms.PictureBox()
        Me.TxbInfosName = New System.Windows.Forms.TextBox()
        Me.GbxAppDirectory = New System.Windows.Forms.GroupBox()
        Me.LblAppDirectory = New System.Windows.Forms.Label()
        Me.TxbAppDirectory = New System.Windows.Forms.TextBox()
        Me.BtnAppDirectory = New System.Windows.Forms.Button()
        Me.GbxShortcut = New System.Windows.Forms.GroupBox()
        Me.PcbShortcutIcon = New System.Windows.Forms.PictureBox()
        Me.LblShortcut = New System.Windows.Forms.Label()
        Me.CbxShortcut = New System.Windows.Forms.ComboBox()
        Me.GbxPostInstall = New System.Windows.Forms.GroupBox()
        Me.PcbPostInstallIcon = New System.Windows.Forms.PictureBox()
        Me.LblPostInstall = New System.Windows.Forms.Label()
        Me.BtnPostInstallRemove = New System.Windows.Forms.Button()
        Me.TxbPostInstall = New System.Windows.Forms.TextBox()
        Me.BtnPostInstallBrowse = New System.Windows.Forms.Button()
        Me.GbxPostDesInstall = New System.Windows.Forms.GroupBox()
        Me.PcbPostDesInstallIcon = New System.Windows.Forms.PictureBox()
        Me.LblPostDesInstall = New System.Windows.Forms.Label()
        Me.BtnPostDesInstallRemove = New System.Windows.Forms.Button()
        Me.TxbPostDesInstall = New System.Windows.Forms.TextBox()
        Me.BtnPostDesInstallBrowse = New System.Windows.Forms.Button()
        Me.GbxArchOS.SuspendLayout()
        Me.GbxAppFile.SuspendLayout()
        CType(Me.PcbAppFileIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxTargetInstall.SuspendLayout()
        Me.GbxInfos.SuspendLayout()
        CType(Me.PcbInfosIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxAppDirectory.SuspendLayout()
        Me.GbxShortcut.SuspendLayout()
        CType(Me.PcbShortcutIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxPostInstall.SuspendLayout()
        CType(Me.PcbPostInstallIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxPostDesInstall.SuspendLayout()
        CType(Me.PcbPostDesInstallIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BgwCreate
        '
        Me.BgwCreate.WorkerReportsProgress = True
        Me.BgwCreate.WorkerSupportsCancellation = True
        '
        'LnkLblBitbucket
        '
        Me.LnkLblBitbucket.AutoSize = True
        Me.LnkLblBitbucket.Location = New System.Drawing.Point(573, 557)
        Me.LnkLblBitbucket.Name = "LnkLblBitbucket"
        Me.LnkLblBitbucket.Size = New System.Drawing.Size(156, 12)
        Me.LnkLblBitbucket.TabIndex = 18
        Me.LnkLblBitbucket.TabStop = True
        Me.LnkLblBitbucket.Text = "https://bitbucket.org/HrtJeanMichel/"
        '
        'GbxArchOS
        '
        Me.GbxArchOS.Controls.Add(Me.CbxArchOS)
        Me.GbxArchOS.Controls.Add(Me.LblArchOS)
        Me.GbxArchOS.Location = New System.Drawing.Point(6, 2)
        Me.GbxArchOS.Name = "GbxArchOS"
        Me.GbxArchOS.Size = New System.Drawing.Size(727, 55)
        Me.GbxArchOS.TabIndex = 19
        Me.GbxArchOS.TabStop = False
        Me.GbxArchOS.Text = "Architecture du système d'exploitation cible"
        '
        'CbxArchOS
        '
        Me.CbxArchOS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxArchOS.FormattingEnabled = True
        Me.CbxArchOS.Items.AddRange(New Object() {"x64 bits", "x32 bits"})
        Me.CbxArchOS.Location = New System.Drawing.Point(113, 22)
        Me.CbxArchOS.Name = "CbxArchOS"
        Me.CbxArchOS.Size = New System.Drawing.Size(473, 20)
        Me.CbxArchOS.TabIndex = 1
        '
        'LblArchOS
        '
        Me.LblArchOS.AutoSize = True
        Me.LblArchOS.Location = New System.Drawing.Point(54, 25)
        Me.LblArchOS.Name = "LblArchOS"
        Me.LblArchOS.Size = New System.Drawing.Size(45, 12)
        Me.LblArchOS.TabIndex = 0
        Me.LblArchOS.Text = "OS Arch :"
        '
        'GbxAppFile
        '
        Me.GbxAppFile.Controls.Add(Me.LblAppFilePath)
        Me.GbxAppFile.Controls.Add(Me.LblAppFileType)
        Me.GbxAppFile.Controls.Add(Me.PcbAppFileIcon)
        Me.GbxAppFile.Controls.Add(Me.TxbAppFileType)
        Me.GbxAppFile.Controls.Add(Me.TxbAppFilePath)
        Me.GbxAppFile.Controls.Add(Me.LblAppFileCpuTarget)
        Me.GbxAppFile.Controls.Add(Me.TxbAppFileCpuTarget)
        Me.GbxAppFile.Controls.Add(Me.BtnAppFilePathBrowse)
        Me.GbxAppFile.Enabled = False
        Me.GbxAppFile.Location = New System.Drawing.Point(6, 118)
        Me.GbxAppFile.Name = "GbxAppFile"
        Me.GbxAppFile.Size = New System.Drawing.Size(727, 76)
        Me.GbxAppFile.TabIndex = 21
        Me.GbxAppFile.TabStop = False
        Me.GbxAppFile.Text = "Choix du fichier de lancement de l'application"
        '
        'LblAppFilePath
        '
        Me.LblAppFilePath.AutoSize = True
        Me.LblAppFilePath.Location = New System.Drawing.Point(22, 27)
        Me.LblAppFilePath.Name = "LblAppFilePath"
        Me.LblAppFilePath.Size = New System.Drawing.Size(75, 12)
        Me.LblAppFilePath.TabIndex = 0
        Me.LblAppFilePath.Text = "Chemin (fichier) :"
        '
        'LblAppFileType
        '
        Me.LblAppFileType.AutoSize = True
        Me.LblAppFileType.Location = New System.Drawing.Point(280, 51)
        Me.LblAppFileType.Name = "LblAppFileType"
        Me.LblAppFileType.Size = New System.Drawing.Size(110, 12)
        Me.LblAppFileType.TabIndex = 5
        Me.LblAppFileType.Text = "Type (Native ou DotNet) :"
        Me.LblAppFileType.Visible = False
        '
        'PcbAppFileIcon
        '
        Me.PcbAppFileIcon.Location = New System.Drawing.Point(673, 10)
        Me.PcbAppFileIcon.Name = "PcbAppFileIcon"
        Me.PcbAppFileIcon.Size = New System.Drawing.Size(48, 44)
        Me.PcbAppFileIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbAppFileIcon.TabIndex = 2
        Me.PcbAppFileIcon.TabStop = False
        '
        'TxbAppFileType
        '
        Me.TxbAppFileType.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbAppFileType.Location = New System.Drawing.Point(396, 48)
        Me.TxbAppFileType.Name = "TxbAppFileType"
        Me.TxbAppFileType.ReadOnly = True
        Me.TxbAppFileType.Size = New System.Drawing.Size(190, 18)
        Me.TxbAppFileType.TabIndex = 6
        Me.TxbAppFileType.Visible = False
        '
        'TxbAppFilePath
        '
        Me.TxbAppFilePath.BackColor = System.Drawing.Color.White
        Me.TxbAppFilePath.Location = New System.Drawing.Point(113, 24)
        Me.TxbAppFilePath.Name = "TxbAppFilePath"
        Me.TxbAppFilePath.ReadOnly = True
        Me.TxbAppFilePath.Size = New System.Drawing.Size(473, 18)
        Me.TxbAppFilePath.TabIndex = 1
        '
        'LblAppFileCpuTarget
        '
        Me.LblAppFileCpuTarget.AutoSize = True
        Me.LblAppFileCpuTarget.Location = New System.Drawing.Point(47, 51)
        Me.LblAppFileCpuTarget.Name = "LblAppFileCpuTarget"
        Me.LblAppFileCpuTarget.Size = New System.Drawing.Size(51, 12)
        Me.LblAppFileCpuTarget.TabIndex = 3
        Me.LblAppFileCpuTarget.Text = "CPU cible :"
        Me.LblAppFileCpuTarget.Visible = False
        '
        'TxbAppFileCpuTarget
        '
        Me.TxbAppFileCpuTarget.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbAppFileCpuTarget.Location = New System.Drawing.Point(113, 48)
        Me.TxbAppFileCpuTarget.Name = "TxbAppFileCpuTarget"
        Me.TxbAppFileCpuTarget.ReadOnly = True
        Me.TxbAppFileCpuTarget.Size = New System.Drawing.Size(161, 18)
        Me.TxbAppFileCpuTarget.TabIndex = 4
        Me.TxbAppFileCpuTarget.Visible = False
        '
        'BtnAppFilePathBrowse
        '
        Me.BtnAppFilePathBrowse.Location = New System.Drawing.Point(592, 22)
        Me.BtnAppFilePathBrowse.Name = "BtnAppFilePathBrowse"
        Me.BtnAppFilePathBrowse.Size = New System.Drawing.Size(75, 21)
        Me.BtnAppFilePathBrowse.TabIndex = 2
        Me.BtnAppFilePathBrowse.Text = "Parcourir"
        Me.BtnAppFilePathBrowse.UseVisualStyleBackColor = True
        '
        'BtnCreate
        '
        Me.BtnCreate.Enabled = False
        Me.BtnCreate.Location = New System.Drawing.Point(6, 522)
        Me.BtnCreate.Name = "BtnCreate"
        Me.BtnCreate.Size = New System.Drawing.Size(727, 33)
        Me.BtnCreate.TabIndex = 26
        Me.BtnCreate.Text = "Créer l'installeur et le désinstalleur"
        Me.BtnCreate.UseVisualStyleBackColor = True
        '
        'GbxTargetInstall
        '
        Me.GbxTargetInstall.Controls.Add(Me.LblTargetInstall)
        Me.GbxTargetInstall.Controls.Add(Me.TxbTargetInstallPath)
        Me.GbxTargetInstall.Enabled = False
        Me.GbxTargetInstall.Location = New System.Drawing.Point(6, 287)
        Me.GbxTargetInstall.Name = "GbxTargetInstall"
        Me.GbxTargetInstall.Size = New System.Drawing.Size(727, 55)
        Me.GbxTargetInstall.TabIndex = 23
        Me.GbxTargetInstall.TabStop = False
        Me.GbxTargetInstall.Text = "Emplacement d'installation future de l'application"
        '
        'LblTargetInstall
        '
        Me.LblTargetInstall.AutoSize = True
        Me.LblTargetInstall.Location = New System.Drawing.Point(6, 25)
        Me.LblTargetInstall.Name = "LblTargetInstall"
        Me.LblTargetInstall.Size = New System.Drawing.Size(89, 12)
        Me.LblTargetInstall.TabIndex = 0
        Me.LblTargetInstall.Text = "Chemin (répertoire) :"
        '
        'TxbTargetInstallPath
        '
        Me.TxbTargetInstallPath.BackColor = System.Drawing.Color.White
        Me.TxbTargetInstallPath.Location = New System.Drawing.Point(113, 22)
        Me.TxbTargetInstallPath.Name = "TxbTargetInstallPath"
        Me.TxbTargetInstallPath.Size = New System.Drawing.Size(473, 18)
        Me.TxbTargetInstallPath.TabIndex = 1
        '
        'PgbCreate
        '
        Me.PgbCreate.Location = New System.Drawing.Point(6, 522)
        Me.PgbCreate.Name = "PgbCreate"
        Me.PgbCreate.Size = New System.Drawing.Size(727, 33)
        Me.PgbCreate.TabIndex = 27
        '
        'GbxInfos
        '
        Me.GbxInfos.Controls.Add(Me.BtnInfosIconRemove)
        Me.GbxInfos.Controls.Add(Me.LblInfosIcon)
        Me.GbxInfos.Controls.Add(Me.TxbInfosIcon)
        Me.GbxInfos.Controls.Add(Me.BtnInfosIcon)
        Me.GbxInfos.Controls.Add(Me.LblInfosManufacturer)
        Me.GbxInfos.Controls.Add(Me.TxbInfosManufacturer)
        Me.GbxInfos.Controls.Add(Me.LblInfosVersion)
        Me.GbxInfos.Controls.Add(Me.TxbInfosVersion)
        Me.GbxInfos.Controls.Add(Me.LblInfosName)
        Me.GbxInfos.Controls.Add(Me.PcbInfosIcon)
        Me.GbxInfos.Controls.Add(Me.TxbInfosName)
        Me.GbxInfos.Enabled = False
        Me.GbxInfos.Location = New System.Drawing.Point(6, 197)
        Me.GbxInfos.Name = "GbxInfos"
        Me.GbxInfos.Size = New System.Drawing.Size(727, 84)
        Me.GbxInfos.TabIndex = 22
        Me.GbxInfos.TabStop = False
        Me.GbxInfos.Text = "Informations de l'application"
        '
        'BtnInfosIconRemove
        '
        Me.BtnInfosIconRemove.BackColor = System.Drawing.Color.MistyRose
        Me.BtnInfosIconRemove.Enabled = False
        Me.BtnInfosIconRemove.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.BtnInfosIconRemove.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.BtnInfosIconRemove.Location = New System.Drawing.Point(564, 47)
        Me.BtnInfosIconRemove.Name = "BtnInfosIconRemove"
        Me.BtnInfosIconRemove.Size = New System.Drawing.Size(22, 20)
        Me.BtnInfosIconRemove.TabIndex = 8
        Me.BtnInfosIconRemove.Text = "X"
        Me.BtnInfosIconRemove.UseVisualStyleBackColor = False
        '
        'LblInfosIcon
        '
        Me.LblInfosIcon.AutoSize = True
        Me.LblInfosIcon.Location = New System.Drawing.Point(67, 51)
        Me.LblInfosIcon.Name = "LblInfosIcon"
        Me.LblInfosIcon.Size = New System.Drawing.Size(33, 12)
        Me.LblInfosIcon.TabIndex = 6
        Me.LblInfosIcon.Text = "Icône :"
        '
        'TxbInfosIcon
        '
        Me.TxbInfosIcon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbInfosIcon.Location = New System.Drawing.Point(113, 48)
        Me.TxbInfosIcon.Name = "TxbInfosIcon"
        Me.TxbInfosIcon.ReadOnly = True
        Me.TxbInfosIcon.Size = New System.Drawing.Size(445, 18)
        Me.TxbInfosIcon.TabIndex = 7
        '
        'BtnInfosIcon
        '
        Me.BtnInfosIcon.Location = New System.Drawing.Point(592, 47)
        Me.BtnInfosIcon.Name = "BtnInfosIcon"
        Me.BtnInfosIcon.Size = New System.Drawing.Size(75, 21)
        Me.BtnInfosIcon.TabIndex = 9
        Me.BtnInfosIcon.Text = "Parcourir"
        Me.BtnInfosIcon.UseVisualStyleBackColor = True
        '
        'LblInfosManufacturer
        '
        Me.LblInfosManufacturer.AutoSize = True
        Me.LblInfosManufacturer.Location = New System.Drawing.Point(396, 27)
        Me.LblInfosManufacturer.Name = "LblInfosManufacturer"
        Me.LblInfosManufacturer.Size = New System.Drawing.Size(39, 12)
        Me.LblInfosManufacturer.TabIndex = 4
        Me.LblInfosManufacturer.Text = "Editeur :"
        '
        'TxbInfosManufacturer
        '
        Me.TxbInfosManufacturer.BackColor = System.Drawing.Color.White
        Me.TxbInfosManufacturer.Location = New System.Drawing.Point(441, 24)
        Me.TxbInfosManufacturer.Name = "TxbInfosManufacturer"
        Me.TxbInfosManufacturer.Size = New System.Drawing.Size(145, 18)
        Me.TxbInfosManufacturer.TabIndex = 5
        '
        'LblInfosVersion
        '
        Me.LblInfosVersion.AutoSize = True
        Me.LblInfosVersion.Location = New System.Drawing.Point(280, 27)
        Me.LblInfosVersion.Name = "LblInfosVersion"
        Me.LblInfosVersion.Size = New System.Drawing.Size(42, 12)
        Me.LblInfosVersion.TabIndex = 2
        Me.LblInfosVersion.Text = "Version :"
        '
        'TxbInfosVersion
        '
        Me.TxbInfosVersion.BackColor = System.Drawing.Color.White
        Me.TxbInfosVersion.Location = New System.Drawing.Point(327, 24)
        Me.TxbInfosVersion.Name = "TxbInfosVersion"
        Me.TxbInfosVersion.Size = New System.Drawing.Size(63, 18)
        Me.TxbInfosVersion.TabIndex = 3
        '
        'LblInfosName
        '
        Me.LblInfosName.AutoSize = True
        Me.LblInfosName.Location = New System.Drawing.Point(72, 27)
        Me.LblInfosName.Name = "LblInfosName"
        Me.LblInfosName.Size = New System.Drawing.Size(30, 12)
        Me.LblInfosName.TabIndex = 0
        Me.LblInfosName.Text = "Nom :"
        '
        'PcbInfosIcon
        '
        Me.PcbInfosIcon.Location = New System.Drawing.Point(673, 38)
        Me.PcbInfosIcon.Name = "PcbInfosIcon"
        Me.PcbInfosIcon.Size = New System.Drawing.Size(48, 44)
        Me.PcbInfosIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbInfosIcon.TabIndex = 2
        Me.PcbInfosIcon.TabStop = False
        '
        'TxbInfosName
        '
        Me.TxbInfosName.BackColor = System.Drawing.Color.White
        Me.TxbInfosName.Location = New System.Drawing.Point(113, 24)
        Me.TxbInfosName.Name = "TxbInfosName"
        Me.TxbInfosName.Size = New System.Drawing.Size(161, 18)
        Me.TxbInfosName.TabIndex = 1
        '
        'GbxAppDirectory
        '
        Me.GbxAppDirectory.Controls.Add(Me.LblAppDirectory)
        Me.GbxAppDirectory.Controls.Add(Me.TxbAppDirectory)
        Me.GbxAppDirectory.Controls.Add(Me.BtnAppDirectory)
        Me.GbxAppDirectory.Location = New System.Drawing.Point(6, 60)
        Me.GbxAppDirectory.Name = "GbxAppDirectory"
        Me.GbxAppDirectory.Size = New System.Drawing.Size(727, 55)
        Me.GbxAppDirectory.TabIndex = 20
        Me.GbxAppDirectory.TabStop = False
        Me.GbxAppDirectory.Text = "Répertoire de l'application"
        '
        'LblAppDirectory
        '
        Me.LblAppDirectory.AutoSize = True
        Me.LblAppDirectory.Location = New System.Drawing.Point(6, 25)
        Me.LblAppDirectory.Name = "LblAppDirectory"
        Me.LblAppDirectory.Size = New System.Drawing.Size(89, 12)
        Me.LblAppDirectory.TabIndex = 0
        Me.LblAppDirectory.Text = "Chemin (répertoire) :"
        '
        'TxbAppDirectory
        '
        Me.TxbAppDirectory.BackColor = System.Drawing.Color.White
        Me.TxbAppDirectory.Location = New System.Drawing.Point(113, 22)
        Me.TxbAppDirectory.Name = "TxbAppDirectory"
        Me.TxbAppDirectory.ReadOnly = True
        Me.TxbAppDirectory.Size = New System.Drawing.Size(473, 18)
        Me.TxbAppDirectory.TabIndex = 1
        '
        'BtnAppDirectory
        '
        Me.BtnAppDirectory.Location = New System.Drawing.Point(592, 20)
        Me.BtnAppDirectory.Name = "BtnAppDirectory"
        Me.BtnAppDirectory.Size = New System.Drawing.Size(75, 21)
        Me.BtnAppDirectory.TabIndex = 3
        Me.BtnAppDirectory.Text = "Parcourir"
        Me.BtnAppDirectory.UseVisualStyleBackColor = True
        '
        'GbxShortcut
        '
        Me.GbxShortcut.Controls.Add(Me.PcbShortcutIcon)
        Me.GbxShortcut.Controls.Add(Me.LblShortcut)
        Me.GbxShortcut.Controls.Add(Me.CbxShortcut)
        Me.GbxShortcut.Enabled = False
        Me.GbxShortcut.Location = New System.Drawing.Point(6, 345)
        Me.GbxShortcut.Name = "GbxShortcut"
        Me.GbxShortcut.Size = New System.Drawing.Size(727, 55)
        Me.GbxShortcut.TabIndex = 24
        Me.GbxShortcut.TabStop = False
        Me.GbxShortcut.Text = "Création d'un raccourci d'application"
        '
        'PcbShortcutIcon
        '
        Me.PcbShortcutIcon.Location = New System.Drawing.Point(673, 9)
        Me.PcbShortcutIcon.Name = "PcbShortcutIcon"
        Me.PcbShortcutIcon.Size = New System.Drawing.Size(48, 44)
        Me.PcbShortcutIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbShortcutIcon.TabIndex = 16
        Me.PcbShortcutIcon.TabStop = False
        '
        'LblShortcut
        '
        Me.LblShortcut.AutoSize = True
        Me.LblShortcut.Location = New System.Drawing.Point(30, 27)
        Me.LblShortcut.Name = "LblShortcut"
        Me.LblShortcut.Size = New System.Drawing.Size(67, 12)
        Me.LblShortcut.TabIndex = 0
        Me.LblShortcut.Text = "Emplacement :"
        '
        'CbxShortcut
        '
        Me.CbxShortcut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxShortcut.FormattingEnabled = True
        Me.CbxShortcut.ItemHeight = 12
        Me.CbxShortcut.Items.AddRange(New Object() {"<Aucun>", "Bureau", "Menu démarrer"})
        Me.CbxShortcut.Location = New System.Drawing.Point(113, 24)
        Me.CbxShortcut.Name = "CbxShortcut"
        Me.CbxShortcut.Size = New System.Drawing.Size(473, 20)
        Me.CbxShortcut.TabIndex = 1
        '
        'GbxPostInstall
        '
        Me.GbxPostInstall.Controls.Add(Me.PcbPostInstallIcon)
        Me.GbxPostInstall.Controls.Add(Me.LblPostInstall)
        Me.GbxPostInstall.Controls.Add(Me.BtnPostInstallRemove)
        Me.GbxPostInstall.Controls.Add(Me.TxbPostInstall)
        Me.GbxPostInstall.Controls.Add(Me.BtnPostInstallBrowse)
        Me.GbxPostInstall.Enabled = False
        Me.GbxPostInstall.Location = New System.Drawing.Point(6, 403)
        Me.GbxPostInstall.Name = "GbxPostInstall"
        Me.GbxPostInstall.Size = New System.Drawing.Size(727, 55)
        Me.GbxPostInstall.TabIndex = 25
        Me.GbxPostInstall.TabStop = False
        Me.GbxPostInstall.Text = "Exécuter un script post-installation (.bat ou .cmd)"
        '
        'PcbPostInstallIcon
        '
        Me.PcbPostInstallIcon.Location = New System.Drawing.Point(673, 9)
        Me.PcbPostInstallIcon.Name = "PcbPostInstallIcon"
        Me.PcbPostInstallIcon.Size = New System.Drawing.Size(48, 44)
        Me.PcbPostInstallIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbPostInstallIcon.TabIndex = 18
        Me.PcbPostInstallIcon.TabStop = False
        '
        'LblPostInstall
        '
        Me.LblPostInstall.AutoSize = True
        Me.LblPostInstall.Location = New System.Drawing.Point(22, 26)
        Me.LblPostInstall.Name = "LblPostInstall"
        Me.LblPostInstall.Size = New System.Drawing.Size(75, 12)
        Me.LblPostInstall.TabIndex = 0
        Me.LblPostInstall.Text = "Chemin (fichier) :"
        '
        'BtnPostInstallRemove
        '
        Me.BtnPostInstallRemove.BackColor = System.Drawing.Color.MistyRose
        Me.BtnPostInstallRemove.Enabled = False
        Me.BtnPostInstallRemove.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.BtnPostInstallRemove.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.BtnPostInstallRemove.Location = New System.Drawing.Point(564, 21)
        Me.BtnPostInstallRemove.Name = "BtnPostInstallRemove"
        Me.BtnPostInstallRemove.Size = New System.Drawing.Size(22, 20)
        Me.BtnPostInstallRemove.TabIndex = 2
        Me.BtnPostInstallRemove.Text = "X"
        Me.BtnPostInstallRemove.UseVisualStyleBackColor = False
        '
        'TxbPostInstall
        '
        Me.TxbPostInstall.BackColor = System.Drawing.Color.White
        Me.TxbPostInstall.Location = New System.Drawing.Point(113, 23)
        Me.TxbPostInstall.Name = "TxbPostInstall"
        Me.TxbPostInstall.ReadOnly = True
        Me.TxbPostInstall.Size = New System.Drawing.Size(445, 18)
        Me.TxbPostInstall.TabIndex = 1
        '
        'BtnPostInstallBrowse
        '
        Me.BtnPostInstallBrowse.Location = New System.Drawing.Point(592, 21)
        Me.BtnPostInstallBrowse.Name = "BtnPostInstallBrowse"
        Me.BtnPostInstallBrowse.Size = New System.Drawing.Size(75, 21)
        Me.BtnPostInstallBrowse.TabIndex = 3
        Me.BtnPostInstallBrowse.Text = "Parcourir"
        Me.BtnPostInstallBrowse.UseVisualStyleBackColor = True
        '
        'GbxPostDesInstall
        '
        Me.GbxPostDesInstall.Controls.Add(Me.PcbPostDesInstallIcon)
        Me.GbxPostDesInstall.Controls.Add(Me.LblPostDesInstall)
        Me.GbxPostDesInstall.Controls.Add(Me.BtnPostDesInstallRemove)
        Me.GbxPostDesInstall.Controls.Add(Me.TxbPostDesInstall)
        Me.GbxPostDesInstall.Controls.Add(Me.BtnPostDesInstallBrowse)
        Me.GbxPostDesInstall.Enabled = False
        Me.GbxPostDesInstall.Location = New System.Drawing.Point(6, 462)
        Me.GbxPostDesInstall.Name = "GbxPostDesInstall"
        Me.GbxPostDesInstall.Size = New System.Drawing.Size(727, 55)
        Me.GbxPostDesInstall.TabIndex = 26
        Me.GbxPostDesInstall.TabStop = False
        Me.GbxPostDesInstall.Text = "Exécuter un script post-desinstallation (.bat ou .cmd)"
        '
        'PcbPostDesInstallIcon
        '
        Me.PcbPostDesInstallIcon.Location = New System.Drawing.Point(673, 9)
        Me.PcbPostDesInstallIcon.Name = "PcbPostDesInstallIcon"
        Me.PcbPostDesInstallIcon.Size = New System.Drawing.Size(48, 44)
        Me.PcbPostDesInstallIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbPostDesInstallIcon.TabIndex = 18
        Me.PcbPostDesInstallIcon.TabStop = False
        '
        'LblPostDesInstall
        '
        Me.LblPostDesInstall.AutoSize = True
        Me.LblPostDesInstall.Location = New System.Drawing.Point(22, 26)
        Me.LblPostDesInstall.Name = "LblPostDesInstall"
        Me.LblPostDesInstall.Size = New System.Drawing.Size(75, 12)
        Me.LblPostDesInstall.TabIndex = 0
        Me.LblPostDesInstall.Text = "Chemin (fichier) :"
        '
        'BtnPostDesInstallRemove
        '
        Me.BtnPostDesInstallRemove.BackColor = System.Drawing.Color.MistyRose
        Me.BtnPostDesInstallRemove.Enabled = False
        Me.BtnPostDesInstallRemove.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.BtnPostDesInstallRemove.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.BtnPostDesInstallRemove.Location = New System.Drawing.Point(564, 21)
        Me.BtnPostDesInstallRemove.Name = "BtnPostDesInstallRemove"
        Me.BtnPostDesInstallRemove.Size = New System.Drawing.Size(22, 20)
        Me.BtnPostDesInstallRemove.TabIndex = 2
        Me.BtnPostDesInstallRemove.Text = "X"
        Me.BtnPostDesInstallRemove.UseVisualStyleBackColor = False
        '
        'TxbPostDesInstall
        '
        Me.TxbPostDesInstall.BackColor = System.Drawing.Color.White
        Me.TxbPostDesInstall.Location = New System.Drawing.Point(113, 23)
        Me.TxbPostDesInstall.Name = "TxbPostDesInstall"
        Me.TxbPostDesInstall.ReadOnly = True
        Me.TxbPostDesInstall.Size = New System.Drawing.Size(445, 18)
        Me.TxbPostDesInstall.TabIndex = 1
        '
        'BtnPostDesInstallBrowse
        '
        Me.BtnPostDesInstallBrowse.Location = New System.Drawing.Point(592, 21)
        Me.BtnPostDesInstallBrowse.Name = "BtnPostDesInstallBrowse"
        Me.BtnPostDesInstallBrowse.Size = New System.Drawing.Size(75, 21)
        Me.BtnPostDesInstallBrowse.TabIndex = 3
        Me.BtnPostDesInstallBrowse.Text = "Parcourir"
        Me.BtnPostDesInstallBrowse.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(738, 569)
        Me.Controls.Add(Me.GbxPostDesInstall)
        Me.Controls.Add(Me.GbxArchOS)
        Me.Controls.Add(Me.GbxAppFile)
        Me.Controls.Add(Me.BtnCreate)
        Me.Controls.Add(Me.GbxTargetInstall)
        Me.Controls.Add(Me.PgbCreate)
        Me.Controls.Add(Me.GbxInfos)
        Me.Controls.Add(Me.GbxAppDirectory)
        Me.Controls.Add(Me.GbxShortcut)
        Me.Controls.Add(Me.GbxPostInstall)
        Me.Controls.Add(Me.LnkLblBitbucket)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Portable SETUP Creator"
        Me.GbxArchOS.ResumeLayout(False)
        Me.GbxArchOS.PerformLayout()
        Me.GbxAppFile.ResumeLayout(False)
        Me.GbxAppFile.PerformLayout()
        CType(Me.PcbAppFileIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxTargetInstall.ResumeLayout(False)
        Me.GbxTargetInstall.PerformLayout()
        Me.GbxInfos.ResumeLayout(False)
        Me.GbxInfos.PerformLayout()
        CType(Me.PcbInfosIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxAppDirectory.ResumeLayout(False)
        Me.GbxAppDirectory.PerformLayout()
        Me.GbxShortcut.ResumeLayout(False)
        Me.GbxShortcut.PerformLayout()
        CType(Me.PcbShortcutIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxPostInstall.ResumeLayout(False)
        Me.GbxPostInstall.PerformLayout()
        CType(Me.PcbPostInstallIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxPostDesInstall.ResumeLayout(False)
        Me.GbxPostDesInstall.PerformLayout()
        CType(Me.PcbPostDesInstallIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BgwCreate As System.ComponentModel.BackgroundWorker
    Friend WithEvents LnkLblBitbucket As LinkLabel
    Friend WithEvents GbxArchOS As GroupBox
    Friend WithEvents CbxArchOS As ComboBox
    Friend WithEvents LblArchOS As Label
    Friend WithEvents GbxAppFile As GroupBox
    Friend WithEvents LblAppFilePath As Label
    Friend WithEvents LblAppFileType As Label
    Friend WithEvents PcbAppFileIcon As PictureBox
    Friend WithEvents TxbAppFileType As TextBox
    Friend WithEvents TxbAppFilePath As TextBox
    Friend WithEvents LblAppFileCpuTarget As Label
    Friend WithEvents TxbAppFileCpuTarget As TextBox
    Friend WithEvents BtnAppFilePathBrowse As Button
    Friend WithEvents BtnCreate As Button
    Friend WithEvents GbxTargetInstall As GroupBox
    Friend WithEvents LblTargetInstall As Label
    Friend WithEvents TxbTargetInstallPath As TextBox
    Friend WithEvents PgbCreate As ProgressBar
    Friend WithEvents GbxInfos As GroupBox
    Friend WithEvents BtnInfosIconRemove As Button
    Friend WithEvents LblInfosIcon As Label
    Friend WithEvents TxbInfosIcon As TextBox
    Friend WithEvents BtnInfosIcon As Button
    Friend WithEvents LblInfosManufacturer As Label
    Friend WithEvents TxbInfosManufacturer As TextBox
    Friend WithEvents LblInfosName As Label
    Friend WithEvents PcbInfosIcon As PictureBox
    Friend WithEvents TxbInfosName As TextBox
    Friend WithEvents GbxAppDirectory As GroupBox
    Friend WithEvents LblAppDirectory As Label
    Friend WithEvents TxbAppDirectory As TextBox
    Friend WithEvents BtnAppDirectory As Button
    Friend WithEvents GbxShortcut As GroupBox
    Friend WithEvents PcbShortcutIcon As PictureBox
    Friend WithEvents LblShortcut As Label
    Friend WithEvents CbxShortcut As ComboBox
    Friend WithEvents GbxPostInstall As GroupBox
    Friend WithEvents PcbPostInstallIcon As PictureBox
    Friend WithEvents LblPostInstall As Label
    Friend WithEvents BtnPostInstallRemove As Button
    Friend WithEvents TxbPostInstall As TextBox
    Friend WithEvents BtnPostInstallBrowse As Button
    Friend WithEvents GbxPostDesInstall As GroupBox
    Friend WithEvents PcbPostDesInstallIcon As PictureBox
    Friend WithEvents LblPostDesInstall As Label
    Friend WithEvents BtnPostDesInstallRemove As Button
    Friend WithEvents TxbPostDesInstall As TextBox
    Friend WithEvents BtnPostDesInstallBrowse As Button
    Friend WithEvents LblInfosVersion As Label
    Friend WithEvents TxbInfosVersion As TextBox
End Class
