﻿Imports System.Media

Public Class DlgQuestion

#Region " Constructor "
    Public Sub New(Title As String, Message As String)
        InitializeComponent()
        Text = Title
        If Message.StartsWith("INFO") Then
            PcbPop.Image = Bitmap.FromHicon(SystemIcons.Information.Handle)
            LblPopMessage.Text = Message.Replace("INFO : ", "")
        ElseIf Message.StartsWith("AVERTISSEMENT") Then
            PcbPop.Image = Bitmap.FromHicon(SystemIcons.Warning.Handle)
            LblPopMessage.Text = Message.Replace("AVERTISSEMENT : ", "")
        ElseIf Message.StartsWith("ERREUR") Then
            PcbPop.Image = Bitmap.FromHicon(SystemIcons.Error.Handle)
            LblPopMessage.Text = Message.Replace("ERREUR : ", "")
        End If
    End Sub
#End Region

#Region " Methods "
    Private Sub DlgPop_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        SystemSounds.Exclamation.Play()
    End Sub

    Private Sub OK_Button_Click(sender As Object, e As EventArgs) Handles Yes_Button.Click
        DialogResult = DialogResult.Yes
        Close()
    End Sub

    Private Sub No_Button_Click(sender As Object, e As EventArgs) Handles No_Button.Click
        DialogResult = DialogResult.No
        Close()
    End Sub

#End Region

End Class
