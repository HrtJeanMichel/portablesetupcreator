﻿Public Interface IMainForm

#Region " Properties "
    Property Presenter As Presenter
    Property OsArchTarget As Integer
    Property AppFileDefaultIcon As Image
    Property AppFileCpuTarget As String
    Property AppFileType As String
    Property AppFileInfosName As String
    Property AppFileInfosVersion As String
    Property AppFileInfosManufacturer As String
    Property AppFileInfosCustomIconPath As String
    Property AppFileInfosCustomIcon As Image
    Property TargetInstallPath As String
    Property ShortcutSelectedIndex As Integer
    Property PostInstallScriptPath As String
    Property PostInstallScriptIcon As Image
    Property PostDesInstallScriptPath As String
    Property PostDesInstallScriptIcon As Image
    Property CreateButtonEnabled As Boolean
#End Region

#Region " WriteOnly Properties "
    WriteOnly Property OSArchTargetGroupEnabled As Boolean
    WriteOnly Property AppRootDirectoryGroupEnabled As Boolean
    WriteOnly Property AppRootDirectoryPath As String
    WriteOnly Property AppFilePath As String
    WriteOnly Property AppFileGroupEnabled As Boolean
    WriteOnly Property AppFileCpuTargetVisible As Boolean
    WriteOnly Property AppFileTypeVisible As Boolean
    WriteOnly Property AppFileInfosCustomIconRemoveButtonEnabled As Boolean
    WriteOnly Property AppFileInfosGroupEnabled As Boolean
    WriteOnly Property TargetInstallPathGroupEnabled As Boolean
    WriteOnly Property ShortcutIcon As Image
    WriteOnly Property ShortcutGroupEnabled As Boolean
    WriteOnly Property PostInstallRemoveButtonEnabled As Boolean
    WriteOnly Property PostInstallScriptGroupEnabled As Boolean
    WriteOnly Property PostDesInstallRemoveButtonEnabled As Boolean
    WriteOnly Property PostDesInstallScriptGroupEnabled As Boolean
    WriteOnly Property CreateButtonVisible As Boolean
    WriteOnly Property CreateProgressVisible As Boolean
#End Region

#Region " Methods "
    Sub ShowCreateProgress(Value As Integer)
#End Region

End Interface
