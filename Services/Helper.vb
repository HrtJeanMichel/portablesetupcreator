﻿Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Runtime.Versioning
Imports System.Text.RegularExpressions
Imports System.Windows.Media.Imaging
Imports Microsoft.Win32

Public NotInheritable Class Helper

#Region " Enums "
    Public Enum MachineType
        Native = 0
        I386 = &H14C
        Itanium = &H200
        x64 = &H8664
    End Enum
#End Region

#Region " Public Methods "
    Public Shared Function IsValidRootPath(Fullpath As String) As Boolean
        Try
            Path.GetFullPath(Fullpath)
            If Fullpath.Contains("\\") OrElse Fullpath.Contains("/") Then
                Return False
            End If
            Return Path.IsPathRooted(Fullpath) AndAlso Not Fullpath.StartsWith("\") AndAlso Not Fullpath.EndsWith(":") AndAlso Not Fullpath.EndsWith(":\") AndAlso Fullpath.Contains("\")
        Catch ex As Exception
        End Try
        Return False
    End Function

    Public Shared Function DeleteExistingFiles(dInfo As DirectoryInfo) As Boolean
        Dim ContainsConfigFiles As Boolean
        For Each fi In dInfo.GetFiles
            If (fi.Name.ToLower.StartsWith("install-") AndAlso fi.Name.ToLower.EndsWith(".exe") AndAlso fi.Name.Length = 44) OrElse
                (fi.Name.ToLower.StartsWith("uninstall-") AndAlso fi.Name.ToLower.EndsWith(".exe") AndAlso fi.Name.Length = 46) OrElse
                (fi.Name.ToLower.StartsWith("ico-") AndAlso fi.Name.ToLower.EndsWith(".ico") AndAlso fi.Name.Length = 40) Then
                ContainsConfigFiles = True
                Exit For
            End If
        Next

        If ContainsConfigFiles Then
            If MessageBox.Show("Des fichiers d'installeur ont été détectés :" & vbNewLine & "- Install-***.exe" & vbNewLine & "- Uninstall-***.exe" & vbNewLine & "- Ico-***.ico)" & vbNewLine & vbNewLine & "Voulez-vous les supprimer et créer un nouvel installeur ?", "Fichier(s) d'installeur existant(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                For Each fi In dInfo.GetFiles
                    If (fi.Name.ToLower.StartsWith("install-") AndAlso fi.Name.ToLower.EndsWith(".exe") AndAlso fi.Name.Length = 44) Then
                        fi.Delete()
                    ElseIf (fi.Name.ToLower.StartsWith("uninstall-") AndAlso fi.Name.ToLower.EndsWith(".exe") AndAlso fi.Name.Length = 46) Then
                        fi.Delete()
                    ElseIf (fi.Name.ToLower.StartsWith("ico-") AndAlso fi.Name.ToLower.EndsWith(".ico") AndAlso fi.Name.Length = 40) Then
                        fi.Delete()
                    End If
                Next
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Public Shared Function SetIconFromIconFilePath(IconPath As String) As Image
        Dim HighIcon = IconUtil.Split(New Icon(IconPath)).OrderByDescending(Function(ico) ico.Size.Width).OrderByDescending(Function(i) IconUtil.GetBitCount(i)).ToList(0)
        Return Compositing(GetAutoSizeImage(PngFromIcon(HighIcon, IconPath), New Size(32, 32)), My.Resources.OverlayIcon32, 0, 0)
    End Function

    Public Shared Function SetIconFromExeFilePath(IconPath As String, Optional ByVal WithExtractor As Boolean = False) As Image
        Try
            If WithExtractor Then
                Dim iconExtract As New IconExtractor(IconPath)
                Dim HighIcon = IconUtil.Split(iconExtract.GetIcon(0)).OrderByDescending(Function(ico) ico.Size.Width).OrderByDescending(Function(i) IconUtil.GetBitCount(i)).ToList(0)
                Dim PngFromIco = PngFromIcon(HighIcon, IconPath)
                Dim ThumbImg = GetAutoSizeImage(PngFromIco, New Size(32, 32))
                Return ThumbImg
            Else
                Dim HighIcon = Icon.ExtractAssociatedIcon(IconPath)
                Return Compositing(HighIcon.ToBitmap, My.Resources.OverlayIcon32, 0, 0)
            End If
        Catch ex As Exception
        End Try
        Return Nothing
    End Function

    Public Shared Function GetDefaultBrowserPath() As String
        Dim defaultBrowserPath As String
        Dim regkey As RegistryKey = Nothing
        Dim subKeyN As String = "IE.HTTP"
        Try
            Dim OS As OperatingSystem = Environment.OSVersion
            If (OS.Platform = PlatformID.Win32NT) AndAlso (OS.Version.Major >= 6) Then
                regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\shell\Associations\UrlAssociations\http\UserChoice", False)
                If regkey IsNot Nothing Then
                    defaultBrowserPath = regkey.GetValue("Progid").ToString()
                Else
                    regkey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Classes\" & subKeyN & "\shell\open\command", False)
                    defaultBrowserPath = regkey.GetValue("").ToString()
                End If
                regkey = Registry.ClassesRoot.OpenSubKey(If(defaultBrowserPath Is Nothing, subKeyN, defaultBrowserPath) & "\Application", False)
                defaultBrowserPath = regkey.GetValue("ApplicationIcon").ToString()
            Else
                regkey = Registry.ClassesRoot.OpenSubKey("http\DefaultIcon", False)
                defaultBrowserPath = regkey.GetValue("").ToString()
            End If
            If Not regkey Is Nothing Then regkey.Close()
        Catch ex As Exception
            If Not regkey Is Nothing Then regkey.Close()
            Return ""
        End Try
        Return If(defaultBrowserPath.Trim(Chr(34)).Contains(","), defaultBrowserPath.Trim(Chr(34)).Split(",")(0), defaultBrowserPath.Trim(Chr(34)))
    End Function

    Public Shared Function IsValidTargetInstallPath(Fullpath As String) As Boolean
        If IsValidRootPath(Fullpath) Then
            Dim driveCheck As Regex = New Regex("^[a-zA-Z]:\\$")
            If Not driveCheck.IsMatch(Fullpath.Substring(0, 3)) Then Return False
            Dim strTheseAreInvalidFileNameChars As String = New String(Path.GetInvalidPathChars())
            strTheseAreInvalidFileNameChars &= ":/?*" & """"
            Dim containsABadCharacter As Regex = New Regex("[" & Regex.Escape(strTheseAreInvalidFileNameChars) & "]")
            If containsABadCharacter.IsMatch(Fullpath.Substring(3, Fullpath.Length - 3)) Then Return False
            If Fullpath.Contains("\") Then
                Dim NumOccurences = Fullpath.Split("\")
                If NumOccurences.Count > 0 Then
                    If NumOccurences.Any(Function(x)
                                             Return String.IsNullOrWhiteSpace(x)
                                         End Function) Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            End If
            Return True
        End If
        Return False
    End Function

    Public Shared Function RemoveInvalidPathChars(str As String) As String
        Dim strTheseAreInvalidFileNameChars As String = New String(Path.GetInvalidPathChars())
        strTheseAreInvalidFileNameChars &= ":/?*" & """"

        Dim NewStr As String = str
        For Each invalidChar In strTheseAreInvalidFileNameChars
            If str.Contains(invalidChar) Then
                NewStr = str.Replace(invalidChar, "")
            End If
        Next
        Return NewStr
    End Function

    Public Shared Function IsInvalidPathChars(Fullpath As String) As Boolean
        If String.IsNullOrWhiteSpace(Fullpath) Then Return True
        Dim strTheseAreInvalidFileNameChars As String = New String(Path.GetInvalidPathChars())
        strTheseAreInvalidFileNameChars &= ":/?*" & """"
        Dim containsABadCharacter As Regex = New Regex("[" & Regex.Escape(strTheseAreInvalidFileNameChars) & "]")
        If containsABadCharacter.IsMatch(Fullpath) Then Return True
        Return False
    End Function

    Public Shared Function CreateStubFromString(MainClass As String, Src As String, IconPath As String, DestFilePath As String, ManifestFilePath As String, CompanyName As String, AppVersion As String, AppName As String) As Boolean
        Try
            Dim nam = DestFilePath
            Dim VersionCompiler = New Dictionary(Of String, String) From {{"CompilerVersion", "v4.0"}}

            Dim NewVersion As Version = Nothing
            If Version.TryParse(AppVersion, NewVersion) Then
                AppVersion = NewVersion.ToString
            Else
                AppVersion = "1.0.0.0"
            End If

            Dim unit = New CodeCompileUnit()
            Dim attr = New CodeTypeReference(GetType(AssemblyVersionAttribute))
            Dim decl = New CodeAttributeDeclaration(attr, New CodeAttributeArgument(New CodePrimitiveExpression(AppVersion)))
            unit.AssemblyCustomAttributes.Add(decl)
            Dim attr1 = New CodeTypeReference(GetType(AssemblyCompanyAttribute))
            Dim decl1 = New CodeAttributeDeclaration(attr1, New CodeAttributeArgument(New CodePrimitiveExpression(CompanyName)))
            unit.AssemblyCustomAttributes.Add(decl1)
            Dim attr2 = New CodeTypeReference(GetType(AssemblyProductAttribute))
            Dim decl2 = New CodeAttributeDeclaration(attr2, New CodeAttributeArgument(New CodePrimitiveExpression(AppName)))
            unit.AssemblyCustomAttributes.Add(decl2)
            Dim attr3 = New CodeTypeReference(GetType(AssemblyFileVersionAttribute))
            Dim decl3 = New CodeAttributeDeclaration(attr3, New CodeAttributeArgument(New CodePrimitiveExpression(AppVersion)))
            unit.AssemblyCustomAttributes.Add(decl3)
            Dim attr4 = New CodeTypeReference(GetType(AssemblyTitleAttribute))
            Dim decl4 = New CodeAttributeDeclaration(attr4, New CodeAttributeArgument(New CodePrimitiveExpression(AppName)))
            unit.AssemblyCustomAttributes.Add(decl4)
            Dim attr5 = New CodeTypeReference(GetType(GuidAttribute))
            Dim decl5 = New CodeAttributeDeclaration(attr5, New CodeAttributeArgument(New CodePrimitiveExpression(Guid.NewGuid.ToString)))
            unit.AssemblyCustomAttributes.Add(decl5)
            Dim attr6 = New CodeTypeReference(GetType(ComVisibleAttribute))
            Dim decl6 = New CodeAttributeDeclaration(attr6, New CodeAttributeArgument(New CodePrimitiveExpression(False)))
            unit.AssemblyCustomAttributes.Add(decl6)
            Dim attr7 = New CodeTypeReference(GetType(TargetFrameworkAttribute))
            Dim decl7 = New CodeAttributeDeclaration(attr7)
            decl7.Arguments.Add(New CodeAttributeArgument(New CodePrimitiveExpression(".NETFramework,Version=v4.0")))
            decl7.Arguments.Add(New CodeAttributeArgument("FrameworkDisplayName", New CodePrimitiveExpression(".NET Framework 4")))
            unit.AssemblyCustomAttributes.Add(decl7)
            Dim attr8 = New CodeTypeReference(GetType(AssemblyDescriptionAttribute))
            Dim decl8 = New CodeAttributeDeclaration(attr8, New CodeAttributeArgument(New CodePrimitiveExpression("")))
            unit.AssemblyCustomAttributes.Add(decl8)
            Dim attr9 = New CodeTypeReference(GetType(AssemblyCopyrightAttribute))
            Dim decl9 = New CodeAttributeDeclaration(attr9, New CodeAttributeArgument(New CodePrimitiveExpression("")))
            unit.AssemblyCustomAttributes.Add(decl9)
            Dim attr10 = New CodeTypeReference(GetType(AssemblyTrademarkAttribute))
            Dim decl10 = New CodeAttributeDeclaration(attr10, New CodeAttributeArgument(New CodePrimitiveExpression("")))
            unit.AssemblyCustomAttributes.Add(decl10)

            Dim assemblyInfo = New StringWriter()

            Dim cProv As New VBCodeProvider(VersionCompiler)
            cProv.GenerateCodeFromCompileUnit(unit, assemblyInfo, New CodeGeneratorOptions())
            Dim cParams As New CompilerParameters()
            With cParams
                With cParams.ReferencedAssemblies
                    .Add("mscorlib.dll")
                    .Add("System.dll")
                End With
                .CompilerOptions = "/target:winexe /platform:anycpu /optimize /win32manifest:" & Chr(34) & ManifestFilePath & Chr(34) & " /win32icon:" & Chr(34) & IconPath & Chr(34)
                .GenerateExecutable = False
                .OutputAssembly = nam
                .GenerateInMemory = False
                .IncludeDebugInformation = False
                .MainClass = MainClass
            End With

            Dim cResults As CompilerResults = cProv.CompileAssemblyFromSource(cParams, {Src, assemblyInfo.ToString})
            If cResults.Errors.Count <> 0 Then
                'For Each er In cResults.Errors
                '    MsgBox("Error on line : " & vbNewLine & er.Line.ToString & vbNewLine &
                '          "Error description : " & er.ErrorText & vbNewLine &
                '          "File : " & vbNewLine & Src)
                'Next
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
            'MsgBox("Error (CreateStubFromString) : " & vbNewLine & ex.ToString)
        End Try
    End Function

    Public Shared Function PngFromIcon(icon As Icon, IconPath As String) As Bitmap
        Try
            Dim png As Bitmap = Nothing

            Using iconStream = New MemoryStream()
                icon.Save(iconStream)
                Dim decoder = New IconBitmapDecoder(iconStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.None)

                Using pngSteam = New MemoryStream()
                    Dim encoder = New PngBitmapEncoder()
                    encoder.Frames.Add(decoder.Frames(0))
                    encoder.Save(pngSteam)
                    png = Bitmap.FromStream(pngSteam)
                End Using
            End Using

            Return png
        Catch ex As Exception
            Return Icon.ExtractAssociatedIcon(IconPath).ToBitmap
        End Try
    End Function

    Public Shared Function GetExeMachineType(fileName As String) As MachineType
        Try
            Const PE_POINTER_OFFSET As Integer = 60
            Const MACHINE_OFFSET As Integer = 4
            Dim data As Byte() = New Byte(4095) {}

            Using s As Stream = New FileStream(fileName, FileMode.Open, FileAccess.Read)
                s.Read(data, 0, 4096)
            End Using

            Dim PE_HEADER_ADDR As Integer = BitConverter.ToInt32(data, PE_POINTER_OFFSET)
            Dim machineUint As Integer = BitConverter.ToUInt16(data, PE_HEADER_ADDR + MACHINE_OFFSET)
            Return CType(machineUint, MachineType)
        Catch ex As Exception
            Return MachineType.I386
        End Try
    End Function

    Public Shared Function FileContentContainsString(FilePath As String, keyword As String) As Boolean
        Dim FileContent = My.Computer.FileSystem.ReadAllText(FilePath)
        If FileContent.ToLower.Contains(keyword.ToLower) Then
            Return True
        End If
        Return False
    End Function

#End Region

#Region " Private Methods "
    Private Shared Function Compositing(ImageFond As Image, ImageOver As Image, y As Integer, x As Integer) As Image
        Dim img As Image = DirectCast(ImageFond.Clone, Image)
        Dim g As Graphics = Graphics.FromImage(img)
        g.SmoothingMode = SmoothingMode.HighQuality
        g.CompositingQuality = CompositingQuality.HighQuality
        g.InterpolationMode = InterpolationMode.High
        g.DrawImage(ImageOver, New Rectangle(x, y, ImageOver.Width, ImageOver.Height))
        g.Flush()
        g.Dispose()
        ImageFond.Dispose()
        ImageOver.Dispose()
        Return img
    End Function

    Private Shared Function GetAutoSizeImage(Img As Bitmap, siz As Size) As Image
        Return Img.GetThumbnailImage(siz.Width, siz.Height, Nothing, Nothing)
    End Function
#End Region

End Class
