﻿Imports System.IO
Imports System.Reflection
Imports System.Reflection.Emit

'/*
' *  IconExtractor/IconUtil for .NET
' *  Copyright (C) 2014 Tsuda Kageyu. All rights reserved.
' *
' *  Redistribution And use in source And binary forms, with Or without
' *  modification, are permitted provided that the following conditions
' *  are met:
' *
' *   1. Redistributions of source code must retain the above copyright
' *      notice, this list of conditions And the following disclaimer.
' *   2. Redistributions in binary form must reproduce the above copyright
' *      notice, this list of conditions And the following disclaimer in the
' *      documentation And/Or other materials provided with the distribution.
' *
' *  THIS SOFTWARE Is PROVIDED BY THE COPYRIGHT HOLDERS And CONTRIBUTORS
' *  "AS IS" And ANY EXPRESS Or IMPLIED WARRANTIES, INCLUDING, BUT Not LIMITED
' *  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY And FITNESS FOR A
' *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
' *  Or CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
' *  EXEMPLARY, Or CONSEQUENTIAL DAMAGES (INCLUDING, BUT Not LIMITED TO,
' *  PROCUREMENT OF SUBSTITUTE GOODS Or SERVICES; LOSS OF USE, DATA, Or
' *  PROFITS; Or BUSINESS INTERRUPTION) HOWEVER CAUSED And ON ANY THEORY OF
' *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, Or TORT (INCLUDING
' *  NEGLIGENCE Or OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
' *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
' */
Public NotInheritable Class IconUtil

    Delegate Function GetIconDataDelegate(icon As Icon) As Byte()
    Shared Property GetIcoData As GetIconDataDelegate

    Shared Sub New()
        Dim dm = New DynamicMethod("GetIcoData", GetType(Byte()), New Type() {GetType(Icon)}, GetType(Icon))
        Dim fi = GetType(Icon).GetField("iconData", BindingFlags.Instance Or BindingFlags.NonPublic)
        Dim gen = dm.GetILGenerator()
        gen.Emit(OpCodes.Ldarg_0)
        gen.Emit(OpCodes.Ldfld, fi)
        gen.Emit(OpCodes.Ret)
        _GetIcoData = CType(dm.CreateDelegate(GetType(GetIconDataDelegate)), GetIconDataDelegate)
    End Sub

    Shared Function Split(icon As Icon) As Icon()
        If icon Is Nothing Then Throw New ArgumentNullException("icon")
        Dim src = GetIconData(icon)
        Dim splitIcons = New List(Of Icon)()

        If True Then
            Dim count As Integer = BitConverter.ToUInt16(src, 4)

            For i As Integer = 0 To count - 1
                Dim length As Integer = BitConverter.ToInt32(src, 6 + 16 * i + 8)
                Dim offset As Integer = BitConverter.ToInt32(src, 6 + 16 * i + 12)

                Using dst = New BinaryWriter(New MemoryStream(6 + 16 + length))
                    dst.Write(src, 0, 4)
                    dst.Write(CShort(1))
                    dst.Write(src, 6 + 16 * i, 12)
                    dst.Write(22)
                    dst.Write(src, offset, length)
                    dst.BaseStream.Seek(0, SeekOrigin.Begin)
                    splitIcons.Add(New Icon(dst.BaseStream))
                End Using
            Next
        End If

        Return splitIcons.ToArray()
    End Function

    Shared Function ToBitmap(icon As Icon) As Bitmap
        If icon Is Nothing Then Throw New ArgumentNullException("icon")

        Using ms = New MemoryStream()
            icon.Save(ms)

            Using bmp = CType(Image.FromStream(ms), Bitmap)
                Return New Bitmap(bmp)
            End Using
        End Using
    End Function

    Shared Function GetBitCount(icon As Icon) As Integer
        If icon Is Nothing Then Throw New ArgumentNullException("icon")
        Dim data = GetIconData(icon)

        If data.Length >= 51 AndAlso data(22) = &H89 AndAlso data(23) = &H50 AndAlso data(24) = &H4E AndAlso data(25) = &H47 AndAlso data(26) = &HD AndAlso data(27) = &HA AndAlso data(28) = &H1A AndAlso data(29) = &HA AndAlso data(30) = &H0 AndAlso data(31) = &H0 AndAlso data(32) = &H0 AndAlso data(33) = &HD AndAlso data(34) = &H49 AndAlso data(35) = &H48 AndAlso data(36) = &H44 AndAlso data(37) = &H52 Then

            Select Case data(47)
                Case 0
                    Return data(46)
                Case 2
                    Return data(46) * 3
                Case 3
                    Return data(46)
                Case 4
                    Return data(46) * 2
                Case 6
                    Return data(46) * 4
                Case Else
            End Select
        ElseIf data.Length >= 22 Then
            Return BitConverter.ToUInt16(data, 12)
        End If

        Throw New ArgumentException("The icon is corrupt. Couldn't read the header.", "icon")
    End Function

    Private Shared Function GetIconData(icon As Icon) As Byte()
        Dim data = _GetIcoData(icon)

        If data IsNot Nothing Then
            Return data
        Else

            Using ms = New MemoryStream()
                icon.Save(ms)
                Return ms.ToArray()
            End Using
        End If
    End Function
End Class

