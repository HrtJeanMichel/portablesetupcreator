﻿Imports System.ComponentModel
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text

'/*
' *  IconExtractor/IconUtil for .NET
' *  Copyright (C) 2014 Tsuda Kageyu. All rights reserved.
' *
' *  Redistribution And use in source And binary forms, with Or without
' *  modification, are permitted provided that the following conditions
' *  are met:
' *
' *   1. Redistributions of source code must retain the above copyright
' *      notice, this list of conditions And the following disclaimer.
' *   2. Redistributions in binary form must reproduce the above copyright
' *      notice, this list of conditions And the following disclaimer in the
' *      documentation And/Or other materials provided with the distribution.
' *
' *  THIS SOFTWARE Is PROVIDED BY THE COPYRIGHT HOLDERS And CONTRIBUTORS
' *  "AS IS" And ANY EXPRESS Or IMPLIED WARRANTIES, INCLUDING, BUT Not LIMITED
' *  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY And FITNESS FOR A
' *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
' *  Or CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
' *  EXEMPLARY, Or CONSEQUENTIAL DAMAGES (INCLUDING, BUT Not LIMITED TO,
' *  PROCUREMENT OF SUBSTITUTE GOODS Or SERVICES; LOSS OF USE, DATA, Or
' *  PROFITS; Or BUSINESS INTERRUPTION) HOWEVER CAUSED And ON ANY THEORY OF
' *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, Or TORT (INCLUDING
' *  NEGLIGENCE Or OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
' *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
' */
Public Class IconExtractor

    Private Const LOAD_LIBRARY_AS_DATAFILE As UInteger = &H2
    Private Shared ReadOnly RT_ICON As IntPtr = CType(3, IntPtr)
    Private Shared ReadOnly RT_GROUP_ICON As IntPtr = CType(14, IntPtr)
    Private Const MAX_PATH As Integer = 260
    Private iconData As Byte()() = Nothing
    Public Property FileName As String

    Public ReadOnly Property Count As Integer
        Get
            Return iconData.Length
        End Get
    End Property

    Public Sub New(fileName As String)
        Initialize(fileName)
    End Sub

    Public Function GetIcon(index As Integer) As Icon
        If index < 0 OrElse Count <= index Then Throw New ArgumentOutOfRangeException("index")

        Using ms = New MemoryStream(iconData(index))
            Return New Icon(ms)
        End Using
    End Function

    Public Function GetAllIcons() As Icon()
        Dim icons = New List(Of Icon)()

        For i As Integer = 0 To Count - 1
            icons.Add(GetIcon(i))
        Next

        Return icons.ToArray()
    End Function

    Private Sub Initialize(fileName As String)
        If fileName Is Nothing Then Throw New ArgumentNullException("fileName")
        Dim hModule As IntPtr = IntPtr.Zero

        Try
            hModule = NativeMethods.LoadLibraryEx(fileName, IntPtr.Zero, LOAD_LIBRARY_AS_DATAFILE)
            If hModule = IntPtr.Zero Then Throw New Win32Exception()
            fileName = GetFileName(hModule)
            Dim tmpData = New List(Of Byte())()
            Dim callback As ENUMRESNAMEPROC = Function(h, t, name, l)
                                                  Dim dir = GetDataFromResource(hModule, RT_GROUP_ICON, name)
                                                  Dim count As Integer = BitConverter.ToUInt16(dir, 4)
                                                  Dim len As Integer = 6 + 16 * count

                                                  For i As Integer = 0 To count - 1
                                                      len += BitConverter.ToInt32(dir, 6 + 14 * i + 8)
                                                  Next

                                                  Using dst = New BinaryWriter(New MemoryStream(len))
                                                      dst.Write(dir, 0, 6)
                                                      Dim picOffset As Integer = 6 + 16 * count

                                                      For i As Integer = 0 To count - 1
                                                          Dim id As UShort = BitConverter.ToUInt16(dir, 6 + 14 * i + 12)
                                                          Dim pic = GetDataFromResource(hModule, RT_ICON, CType(id, IntPtr))
                                                          dst.Seek(6 + 16 * i, SeekOrigin.Begin)
                                                          dst.Write(dir, 6 + 14 * i, 8)
                                                          dst.Write(pic.Length)
                                                          dst.Write(picOffset)
                                                          dst.Seek(picOffset, SeekOrigin.Begin)
                                                          dst.Write(pic, 0, pic.Length)
                                                          picOffset += pic.Length
                                                      Next

                                                      tmpData.Add((CType(dst.BaseStream, MemoryStream)).ToArray())
                                                  End Using

                                                  Return True
                                              End Function

            NativeMethods.EnumResourceNames(hModule, RT_GROUP_ICON, callback, IntPtr.Zero)
            iconData = tmpData.ToArray()
        Finally
            If hModule <> IntPtr.Zero Then NativeMethods.FreeLibrary(hModule)
        End Try
    End Sub

    Private Function GetDataFromResource(hModule As IntPtr, type As IntPtr, name As IntPtr) As Byte()
        Dim hResInfo As IntPtr = NativeMethods.FindResource(hModule, name, type)
        If hResInfo = IntPtr.Zero Then Throw New Win32Exception()
        Dim hResData As IntPtr = NativeMethods.LoadResource(hModule, hResInfo)
        If hResData = IntPtr.Zero Then Throw New Win32Exception()
        Dim pResData As IntPtr = NativeMethods.LockResource(hResData)
        If pResData = IntPtr.Zero Then Throw New Win32Exception()
        Dim size As UInteger = NativeMethods.SizeofResource(hModule, hResInfo)
        If size = 0 Then Throw New Win32Exception()
        Dim buf As Byte() = New Byte(size - 1) {}
        Marshal.Copy(pResData, buf, 0, buf.Length)
        Return buf
    End Function

    Private Function GetFileName(hModule As IntPtr) As String
        Dim FileName As String = ""
        Dim buffer = New StringBuilder(MAX_PATH)
        Dim leng As Integer = NativeMethods.GetMappedFileName(NativeMethods.GetCurrentProcess(), hModule, buffer, buffer.Capacity)
        If leng = 0 Then Throw New Win32Exception()
        FileName = buffer.ToString()


        Dim alphabeticChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray
        For i As Integer = 0 To alphabeticChars.Length - 1
            Dim drive = alphabeticChars(i).ToString & ":"
            Dim buf = New StringBuilder(MAX_PATH)
            Dim len As Integer = NativeMethods.QueryDosDevice(drive, buf, buf.Capacity)
            If len = 0 Then Continue For
            Dim devPath = buf.ToString()
            If FileName.StartsWith(devPath) Then Return (drive & FileName.Substring(devPath.Length))
        Next

        Return FileName
    End Function
End Class
