﻿Imports System.Runtime.InteropServices
Imports System.Security
Imports System.Text

'/*
' *  IconExtractor/IconUtil for .NET
' *  Copyright (C) 2014 Tsuda Kageyu. All rights reserved.
' *
' *  Redistribution And use in source And binary forms, with Or without
' *  modification, are permitted provided that the following conditions
' *  are met:
' *
' *   1. Redistributions of source code must retain the above copyright
' *      notice, this list of conditions And the following disclaimer.
' *   2. Redistributions in binary form must reproduce the above copyright
' *      notice, this list of conditions And the following disclaimer in the
' *      documentation And/Or other materials provided with the distribution.
' *
' *  THIS SOFTWARE Is PROVIDED BY THE COPYRIGHT HOLDERS And CONTRIBUTORS
' *  "AS IS" And ANY EXPRESS Or IMPLIED WARRANTIES, INCLUDING, BUT Not LIMITED
' *  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY And FITNESS FOR A
' *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
' *  Or CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
' *  EXEMPLARY, Or CONSEQUENTIAL DAMAGES (INCLUDING, BUT Not LIMITED TO,
' *  PROCUREMENT OF SUBSTITUTE GOODS Or SERVICES; LOSS OF USE, DATA, Or
' *  PROFITS; Or BUSINESS INTERRUPTION) HOWEVER CAUSED And ON ANY THEORY OF
' *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, Or TORT (INCLUDING
' *  NEGLIGENCE Or OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
' *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
' */
Friend NotInheritable Class NativeMethods

    <DllImport("kernel32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function LoadLibraryEx(lpFileName As String, hFile As IntPtr, dwFlags As UInteger) As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function FreeLibrary(hModule As IntPtr) As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function EnumResourceNames(hModule As IntPtr, lpszType As IntPtr, lpEnumFunc As ENUMRESNAMEPROC, lParam As IntPtr) As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function FindResource(hModule As IntPtr, lpName As IntPtr, lpType As IntPtr) As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function LoadResource(hModule As IntPtr, hResInfo As IntPtr) As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function LockResource(hResData As IntPtr) As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function SizeofResource(hModule As IntPtr, hResInfo As IntPtr) As UInteger
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function GetCurrentProcess() As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function QueryDosDevice(lpDeviceName As String, lpTargetPath As StringBuilder, ucchMax As Integer) As Integer
    End Function

    <DllImport("psapi.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
    <SuppressUnmanagedCodeSecurity>
    Shared Function GetMappedFileName(hProcess As IntPtr, lpv As IntPtr, lpFilename As StringBuilder, nSize As Integer) As Integer
    End Function
End Class

<UnmanagedFunctionPointer(CallingConvention.Winapi, SetLastError:=True, CharSet:=CharSet.Unicode)>
<SuppressUnmanagedCodeSecurity>
Friend Delegate Function ENUMRESNAMEPROC(hModule As IntPtr, lpszType As IntPtr, lpszName As IntPtr, lParam As IntPtr) As Boolean


