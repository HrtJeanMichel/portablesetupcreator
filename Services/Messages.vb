﻿Public NotInheritable Class Messages

#Region " Fields "
    Public Shared SelectRootPath As String = "Sélectionnez le répertoire parent de votre application portable"
    Public Shared SelectAppFile As String = "Sélectionnez le fichier d'exécution (*.exe ou *.html) de votre application portable"
    Public Shared SelectCustomIcon As String = "Sélectionnez un fichier icône personnalisé (*.ico)"
    Public Shared SelectCustomIconHtml As String = "Sélectionnez un fichier icône personnalisé (.ico) (recommandé pour une appli HTML)"
    Public Shared SelectPostInstallScriptFile As String = "Sélectionnez un fichier de script post-installation (*.bat, *.cmd)"
    Public Shared SelectPostDesInstallScriptFile As String = "Sélectionnez un fichier de script post-Desinstallation (*.bat, *.cmd)"
    Public Shared SelectPostScriptPauseDetectionTitle As String = "Contenu incorrect"
    Public Shared SelectPostInstallPauseDetectionMessage As String = "AVERTISSEMENT : Commande PAUSE détectée, l'exécution de ce script mettra le statut de l'installeur en attente d'une saisie, sans aucune possibilité de récupérer un ExitCode ! " & vbNewLine & "Voulez-vous continuer quand même ?"
    Public Shared SelectPostDesInstallPauseDetectionMessage As String = "AVERTISSEMENT : Commande PAUSE détectée, l'exécution de ce script mettra le statut du désinstalleur en attente d'une saisie, sans aucune possibilité de récupérer un ExitCode ! " & vbNewLine & "Voulez-vous continuer quand même ?"

    Public Shared SelectedAppFileNotValid As String = "AVERTISSEMENT : Vous devez sélectionner un fichier contenu dans le répertoire de l'application !"
    Public Shared SelectedRootPathNotValid As String = "AVERTISSEMENT : Vous devez sélectionner un répertoire et non un lecteur !"

    Public Shared InvalidSelection As String = "Sélection invalide"

    Public Shared BinariesCreated As String = "INFO : Les binaires d'installation et de désinstallation ont été créés avec succès.|"
    Public Shared BinaryInstallerError As String = "ERREUR : Une erreur est survenue lors de la génération de l'installeur !"
    Public Shared BinaryUninstallerError As String = "ERREUR : Une erreur est survenue lors de la génération du désinstalleur !"
    Public Shared BinariesNotCreated As String = "ERREUR : Une erreur est survenue lors de la génération de l'installeur et du désinstalleur !"
    Public Shared BinariesCreationError As String = "ERREUR : Une erreur inconnue est survenue !"
    Public Shared BinariesCreationEnded As String = "Fin de l'opération"

    Public Shared SelectedAppManaged As String = "Managed (DotNet)"
    Public Shared SelectedAppUnmanaged As String = "Unmanaged (Native)"
    Public Shared SelectedAppNotValidFor64ArchOS As String = "Le programme sélectionné ne peut pas s'exécuter sur un OS x64 bits !"
    Public Shared SelectedAppNotValidFor32ArchOS As String = "Le programme sélectionné ne peut pas s'exécuter sur un OS x32 bits !"
    Public Shared SelectedAppArchOSNoCorresponding As String = "Aucune correspondance de processeur cible n'a été trouvée !"
    Public Shared SelectedAppErrorParsing As String = "Une erreur est survenue lors de la lecture du fichier !"
    Public Shared SelectedApp86 As String = "x86"
    Public Shared SelectedApp64 As String = "x64"
    Public Shared SelectedAppAny As String = "x86 et x64"

    Public Shared TargetInstallPath As String = "C:\Program Files"
    Public Shared TargetInstallPathx86 As String = "C:\Program Files (x86)"

    Public Shared WindowsTempPath As String = "C:\Windows\Temp"
#End Region

End Class
