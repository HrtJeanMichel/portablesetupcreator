﻿Public Module NativeEnums

#Region " Enums "
    Public Enum MachineType As UShort
        IMAGE_FILE_MACHINE_UNKNOWN = &H0
        IMAGE_FILE_MACHINE_AM33 = &H1D3
        IMAGE_FILE_MACHINE_AMD64 = &H8664
        IMAGE_FILE_MACHINE_ARM = &H1C0
        IMAGE_FILE_MACHINE_EBC = &HEBC
        IMAGE_FILE_MACHINE_I386 = &H14C
        IMAGE_FILE_MACHINE_IA64 = &H200
        IMAGE_FILE_MACHINE_M32R = &H9041
        IMAGE_FILE_MACHINE_MIPS16 = &H266
        IMAGE_FILE_MACHINE_MIPSFPU = &H366
        IMAGE_FILE_MACHINE_MIPSFPU16 = &H466
        IMAGE_FILE_MACHINE_POWERPC = &H1F0
        IMAGE_FILE_MACHINE_POWERPCFP = &H1F1
        IMAGE_FILE_MACHINE_R4000 = &H166
        IMAGE_FILE_MACHINE_SH3 = &H1A2
        IMAGE_FILE_MACHINE_SH3DSP = &H1A3
        IMAGE_FILE_MACHINE_SH4 = &H1A6
        IMAGE_FILE_MACHINE_SH5 = &H1A8
        IMAGE_FILE_MACHINE_THUMB = &H1C2
        IMAGE_FILE_MACHINE_WCEMIPSV2 = &H169
    End Enum
#End Region

End Module
