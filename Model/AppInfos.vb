﻿Public Class AppInfos
    Implements IDisposable

#Region " Properties "
    Public InstallPath As String
    Public AppGuid As String
    Public AppFilePath As String
    Public IsHTML As Boolean
    Public AppDefaultIcon As Image
    Public Description As String
    Public Comments As String
    Public CompanyName As String
    Public ProductName As String
    Public LegalCopyright As String
    Public LegalTrademarks As String
    Public FileVersion As String
    Public CpuTarget As String
    Public FileType As String
#End Region

#Region "IDisposable Support"
    Private disposedValue As Boolean

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
            End If
            AppGuid = String.Empty
            AppFilePath = String.Empty
            AppDefaultIcon = Nothing
            Description = String.Empty
            Comments = String.Empty
            CompanyName = String.Empty
            ProductName = String.Empty
            LegalCopyright = String.Empty
            LegalTrademarks = String.Empty
            FileVersion = String.Empty
            CpuTarget = String.Empty
            FileType = String.Empty
        End If
        disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class