﻿Imports System.ComponentModel

Public Interface IPackage

#Region " Properties "
    Property OsTargetArchitecture As String
    Property AppRootDirectoryPath As String
    Property ApplicationInfos As AppInfos
    Property ApplicationCustomIconPath As String
    Property ApplicationCustomIcon As Image
    Property ShortcutIndex As Integer
    Property ApplicationPostInstallScriptPath As String
    Property ApplicationPostDesInstallScriptPath As String
    ReadOnly Property CreationIsBusy As Boolean
#End Region

#Region " Events "
    Event AppFileValidated(sender As Object, e As ValidatedFile)
    Event CreationPackageProgressChanged(sender As Object, e As ProgressChangedEventArgs)
    Event CreationPackageRunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
#End Region

#Region " Methods "
    Sub ReadFileInfos(FilPath As String)
    Sub ClearFieldsFromAppRootDirectory()
    Sub ClearFieldsFromOSArchTarget()
    Sub SetCustomIcon(FilePath As String, Ico As Image)
    Sub SetInstallPostScript(FilePath As String)
    Sub SetDesInstallPostScript(FilePath As String)
    Sub GenerateFileInfosFromDatas(InfosName As String, InfosVersion As String, InfosManufaturer As String, DestInstallPath As String)
    Sub CreatePackage()
#End Region

End Interface
