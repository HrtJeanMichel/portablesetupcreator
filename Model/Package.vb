﻿Imports System.ComponentModel
Imports System.IO
Imports System.Reflection

Public Class Package
    Implements IPackage

#Region " Fields "
    Private ReadOnly BgwCreate As BackgroundWorker
#End Region

#Region " Properties "
    Property OsTargetArchitecture As String Implements IPackage.OsTargetArchitecture
    Property AppRootDirectoryPath As String Implements IPackage.AppRootDirectoryPath
    Property ApplicationInfos As AppInfos Implements IPackage.ApplicationInfos
    Property ApplicationCustomIconPath As String Implements IPackage.ApplicationCustomIconPath
    Property ApplicationCustomIcon As Image Implements IPackage.ApplicationCustomIcon
    Property ShortcutIndex As Integer Implements IPackage.ShortcutIndex
    Property ApplicationPostInstallScriptPath As String Implements IPackage.ApplicationPostInstallScriptPath
    Property ApplicationPostDesInstallScriptPath As String Implements IPackage.ApplicationPostDesInstallScriptPath

    ReadOnly Property CreationIsBusy As Boolean Implements IPackage.CreationIsBusy
        Get
            Return BgwCreate.IsBusy
        End Get
    End Property
#End Region

#Region " Events "
    Public Event AppFileValidated(sender As Object, e As ValidatedFile) Implements IPackage.AppFileValidated
    Public Event CreationPackageProgressChanged(sender As Object, e As ProgressChangedEventArgs) Implements IPackage.CreationPackageProgressChanged
    Public Event CreationPackageRunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Implements IPackage.CreationPackageRunWorkerCompleted
#End Region

#Region " Constructor "
    Public Sub New()
        BgwCreate = New BackgroundWorker()
        With BgwCreate
            .WorkerReportsProgress = True
            .WorkerSupportsCancellation = False
            AddHandler .DoWork, New DoWorkEventHandler(AddressOf BgwCreate_DoWork)
            AddHandler .ProgressChanged, New ProgressChangedEventHandler(AddressOf BgwCreate_ProgressChanged)
            AddHandler .RunWorkerCompleted, New RunWorkerCompletedEventHandler(AddressOf BgwCreate_RunWorkerCompleted)
        End With
    End Sub
#End Region

#Region " Methods "
    Public Sub ReadFileInfos(FilPath As String) Implements IPackage.ReadFileInfos
        If FilPath.ToLower.EndsWith(".html") OrElse FilPath.ToLower.EndsWith(".htm") Then
            GetHtmlInfos(FilPath)
        ElseIf FilPath.ToLower.EndsWith(".exe") Then
            GetExePeInfos(FilPath, OsTargetArchitecture)
        End If
    End Sub
    Private Sub OnFileValidationTeminated(sender As Object, e As ValidatedFile)
        RaiseEvent AppFileValidated(sender, e)
    End Sub

    Private Sub GetHtmlInfos(exePath As String)
        RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadHtmlInfos(exePath, Messages.SelectedApp86, Messages.TargetInstallPath), ""))
    End Sub

    Private Sub GetExePeInfos(exePath As String, ArchiOS As String)
        Dim isAssembly As Boolean
        Dim ass As Assembly = Nothing
        Try
            Try
                ass = Assembly.LoadFile(exePath)
                isAssembly = True
            Catch ex As Exception
                isAssembly = False
            End Try

            If isAssembly Then
                Dim peKind As PortableExecutableKinds
                Dim machine As ImageFileMachine
                ass.ManifestModule.GetPEKind(peKind, machine)

                Select Case peKind.ToString & ", " & machine.ToString
                    Case "ILOnly, Preferred32Bit, I386"
                        If ArchiOS = "x32" Then
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppManaged, Messages.TargetInstallPath), ""))
                        Else
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppManaged, Messages.TargetInstallPathx86), ""))
                        End If
                    Case "ILOnly, I386"
                        RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedAppAny, Messages.SelectedAppManaged, Messages.TargetInstallPath), ""))
                    Case "ILOnly, Required32Bit, I386"
                        If ArchiOS = "x32" Then
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppManaged, Messages.TargetInstallPath), ""))
                        Else
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppManaged, Messages.TargetInstallPathx86), ""))
                        End If
                    Case "ILOnly, PE32Plus, AMD64"
                        If ArchiOS = "x64" Then
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp64, Messages.SelectedAppManaged, Messages.TargetInstallPath), ""))
                        Else
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(False, Nothing, Messages.SelectedAppNotValidFor32ArchOS))
                        End If
                    Case Else
                        RaiseEvent AppFileValidated(Me, New ValidatedFile(False, Nothing, Messages.SelectedAppArchOSNoCorresponding))
                End Select
            Else
                Select Case Helper.GetExeMachineType(exePath)
                    Case MachineType.IMAGE_FILE_MACHINE_AMD64
                        If ArchiOS = "x64" Then
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp64, Messages.SelectedAppUnmanaged, Messages.TargetInstallPath), ""))
                        Else
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(False, Nothing, Messages.SelectedAppNotValidFor32ArchOS))
                        End If
                    Case MachineType.IMAGE_FILE_MACHINE_I386

                        If ArchiOS = "x32" Then
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppUnmanaged, Messages.TargetInstallPath), ""))
                        Else
                            RaiseEvent AppFileValidated(Me, New ValidatedFile(True, ReadNativeInfos(exePath, Messages.SelectedApp86, Messages.SelectedAppUnmanaged, Messages.TargetInstallPathx86), ""))
                        End If
                    Case Else
                        RaiseEvent AppFileValidated(Me, New ValidatedFile(False, Nothing, Messages.SelectedAppArchOSNoCorresponding))
                End Select
            End If
        Catch ex As Exception
            RaiseEvent AppFileValidated(Me, New ValidatedFile(False, Nothing, Messages.SelectedAppErrorParsing))
        End Try
    End Sub

    Private Function ReadHtmlInfos(FilPath As String, FilArch As String, DestInstallPath As String) As AppInfos
        Dim fi As New AppInfos
        With fi
            .AppGuid = Guid.NewGuid.ToString.Replace("-", "")
            .InstallPath = DestInstallPath
            .AppFilePath = FilPath
            .IsHTML = True
            .AppDefaultIcon = Icon.ExtractAssociatedIcon(FilPath).ToBitmap()
            .Description = ""
            .Comments = ""
            .CompanyName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
            .ProductName = .CompanyName
            .LegalCopyright = ""
            .LegalTrademarks = ""
            .FileVersion = "1.0.0.0"
            .CpuTarget = FilArch
            .FileType = ""
        End With
        Return fi
    End Function

    Private Function ReadNativeInfos(FilPath As String, FilArch As String, FilType As String, DestInstallPath As String) As AppInfos
        Dim fvi As FileVersionInfo = FileVersionInfo.GetVersionInfo(FilPath)
        Dim AppIfo As New AppInfos

        With AppIfo
            .AppGuid = Guid.NewGuid.ToString.Replace("-", "")
            .InstallPath = DestInstallPath
            .AppFilePath = FilPath
            .IsHTML = False
            .AppDefaultIcon = Helper.SetIconFromExeFilePath(FilPath, True)
            .Description = fvi.FileDescription
            .Comments = fvi.Comments
            If fvi.ProductName = "" Then
                AppIfo.ProductName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
            Else
                If String.IsNullOrWhiteSpace(fvi.ProductName.Trim) Then
                    AppIfo.ProductName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
                Else
                    AppIfo.ProductName = Helper.RemoveInvalidPathChars(fvi.ProductName.Trim)
                End If
            End If

            If fvi.CompanyName = "" Then
                AppIfo.CompanyName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
            Else
                If String.IsNullOrWhiteSpace(fvi.CompanyName.Trim) Then
                    AppIfo.CompanyName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
                Else
                    AppIfo.CompanyName = fvi.CompanyName.Trim
                End If
            End If
            .LegalCopyright = fvi.LegalCopyright
            .LegalTrademarks = fvi.LegalTrademarks
            Dim NewVersion As Version = Nothing
            If Version.TryParse(fvi.FileVersion, NewVersion) Then
                .FileVersion = NewVersion.ToString
            Else
                .FileVersion = "1.0.0.0"
            End If
            .CpuTarget = FilArch
            .FileType = FilType
        End With

        Return AppIfo
    End Function

    Public Sub GenerateFileInfosFromDatas(InfosName As String, InfosVersion As String, InfosManufaturer As String, DestInstallPath As String) Implements IPackage.GenerateFileInfosFromDatas
        If String.IsNullOrWhiteSpace(InfosName) Then
            ApplicationInfos.ProductName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
        Else
            ApplicationInfos.ProductName = InfosName
        End If
        If String.IsNullOrWhiteSpace(InfosManufaturer) Then
            ApplicationInfos.CompanyName = New DirectoryInfo(AppRootDirectoryPath).Name.Replace("-", " ").Replace("_", " ")
        Else
            ApplicationInfos.CompanyName = InfosManufaturer
        End If
        Dim NewVersion As Version = Nothing
        If Version.TryParse(InfosVersion, NewVersion) Then
            ApplicationInfos.FileVersion = NewVersion.ToString
        Else
            ApplicationInfos.FileVersion = "1.0.0.0"
        End If
        ApplicationInfos.InstallPath = DestInstallPath
    End Sub

    Private Sub ClearFieldsFromAppRootDirectory() Implements IPackage.ClearFieldsFromAppRootDirectory
        ApplicationInfos = Nothing
        ClearMainFields()
    End Sub

    Private Sub ClearFieldsFromOSArchTarget() Implements IPackage.ClearFieldsFromOSArchTarget
        AppRootDirectoryPath = ""
        ApplicationInfos = Nothing
        ClearMainFields()
    End Sub

    Private Sub ClearMainFields()
        ApplicationCustomIconPath = ""
        ApplicationCustomIcon = Nothing
        ApplicationPostInstallScriptPath = ""
    End Sub

    Public Sub SetCustomIcon(Filepath As String, CustomIcon As Image) Implements IPackage.SetCustomIcon
        ApplicationCustomIcon = CustomIcon
        ApplicationCustomIconPath = Filepath
    End Sub

    Public Sub SetInstallPostScript(FilePath As String) Implements IPackage.SetInstallPostScript
        ApplicationPostInstallScriptPath = FilePath
    End Sub

    Public Sub SetDesInstallPostScript(FilePath As String) Implements IPackage.SetDesInstallPostScript
        ApplicationPostDesInstallScriptPath = FilePath
    End Sub

    Public Sub CreatePackage() Implements IPackage.CreatePackage
        BgwCreate.RunWorkerAsync()
    End Sub

    Private Sub BgwCreate_DoWork(sender As Object, e As DoWorkEventArgs)
        Try
            BgwCreate.ReportProgress(10)

            Dim RootDirectoryPath As String = AppRootDirectoryPath
            Dim RootDirectoryPathDi = New DirectoryInfo(RootDirectoryPath)

            Dim ProgFileName As String = Path.GetFileNameWithoutExtension(ApplicationInfos.AppFilePath)

            Dim AppInstallPath As String = ApplicationInfos.InstallPath
            Dim AppInstallFullPath As String = Path.Combine(AppInstallPath, ApplicationInfos.ProductName)
            Dim AppInstallFullPathFi As FileInfo = New FileInfo(ApplicationInfos.AppFilePath)
            Dim AppInstallFullPathName As String = AppInstallFullPathFi.FullName.Replace(RootDirectoryPath, "").Replace(AppInstallFullPathFi.Name, "").Trim("\")
            Dim DestInstallPath As String = Path.Combine(AppInstallFullPath, AppInstallFullPathName)

            Dim CodedomDestFilePath As String = Path.Combine(RootDirectoryPathDi.FullName, "Install-" & ApplicationInfos.AppGuid & ".exe")
            Dim ResultFile = New FileInfo(CodedomDestFilePath).Name

            Dim CodedomDestFilePathUninst As String = Path.Combine(RootDirectoryPathDi.FullName, "Uninstall-" & ApplicationInfos.AppGuid & ".exe")
            Dim ResultUninstFile = New FileInfo(CodedomDestFilePathUninst).Name

            Dim ExcludeCommandPath As String = Path.Combine(Messages.WindowsTempPath, ApplicationInfos.AppGuid & ".txt")

            Dim ManifestPath As String = Path.Combine(Messages.WindowsTempPath, ApplicationInfos.AppGuid & ".manifest")
            File.WriteAllBytes(ManifestPath, My.Resources.app)

            Dim InstallerIconPath As String = Path.Combine(Messages.WindowsTempPath, "Ico-" & ApplicationInfos.AppGuid & ".ico")
            Using fs As New FileStream(InstallerIconPath, FileMode.Create)
                My.Resources.SETUP.Save(fs)
            End Using

            Dim UninstallerIconPath As String = Path.Combine(Messages.WindowsTempPath, "Uninstall-" & ApplicationInfos.AppGuid & ".ico")
            Using fs As New FileStream(UninstallerIconPath, FileMode.Create)
                My.Resources.UNINSTALL.Save(fs)
            End Using

            Dim ShortcutPath As String = ""
            Dim IconPath As String = ""
            If ShortcutIndex = 1 Then
                ShortcutPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), ApplicationInfos.ProductName & ".lnk")
            ElseIf ShortcutIndex = 2 Then
                ShortcutPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu), ApplicationInfos.ProductName & ".lnk")
            ElseIf ShortcutIndex = 0 Then
                ShortcutPath = ""
            End If

            BgwCreate.ReportProgress(30)

            If ApplicationInfos.IsHTML Then
                If File.Exists(ApplicationCustomIconPath) Then
                    If (New FileInfo(ApplicationCustomIconPath).DirectoryName.ToLower <> RootDirectoryPathDi.FullName.ToLower) Then
                        File.Copy(ApplicationCustomIconPath, Path.Combine(RootDirectoryPathDi.FullName, New FileInfo(ApplicationCustomIconPath).Name), True)
                    End If
                    IconPath = Path.Combine(AppInstallFullPath, New FileInfo(ApplicationCustomIconPath).Name)
                Else
                    Dim iconExtract As New IconExtractor(Helper.GetDefaultBrowserPath)
                    Dim ic = iconExtract.GetIcon(0)
                    Dim HighIcon = IconUtil.Split(ic).OrderByDescending(Function(ico) ico.Size.Width).OrderByDescending(Function(i) IconUtil.GetBitCount(i)).ToList(0)
                    Using fs As New FileStream(Path.Combine(RootDirectoryPathDi.FullName, New FileInfo(InstallerIconPath).Name), FileMode.Create)
                        HighIcon.Save(fs)
                    End Using
                    IconPath = Path.Combine(AppInstallFullPath, New FileInfo(InstallerIconPath).Name)
                End If
                BgwCreate.ReportProgress(50)
            Else
                If File.Exists(ApplicationCustomIconPath) Then
                    If (New FileInfo(ApplicationCustomIconPath).DirectoryName.ToLower <> RootDirectoryPathDi.FullName.ToLower) Then
                        File.Copy(ApplicationCustomIconPath, Path.Combine(RootDirectoryPathDi.FullName, New FileInfo(ApplicationCustomIconPath).Name), True)
                    End If
                    IconPath = Path.Combine(AppInstallFullPath, New FileInfo(ApplicationCustomIconPath).Name)
                Else
                    IconPath = Path.Combine(DestInstallPath, AppInstallFullPathFi.Name) & ",0"
                End If
                BgwCreate.ReportProgress(50)
            End If

            Dim scriptPath = ""
            If File.Exists(ApplicationPostInstallScriptPath) Then
                If (New FileInfo(ApplicationPostInstallScriptPath).DirectoryName.ToLower <> RootDirectoryPathDi.FullName.ToLower) Then
                    File.Copy(ApplicationPostInstallScriptPath, Path.Combine(RootDirectoryPathDi.FullName, New FileInfo(ApplicationPostInstallScriptPath).Name), True)
                End If
                scriptPath = Path.Combine(AppInstallFullPath, New FileInfo(ApplicationPostInstallScriptPath).Name)
                BgwCreate.ReportProgress(60)
            End If

            Dim scriptPostDesInstallPath = ""
            If File.Exists(ApplicationPostDesInstallScriptPath) Then
                If (New FileInfo(ApplicationPostDesInstallScriptPath).DirectoryName.ToLower <> RootDirectoryPathDi.FullName.ToLower) Then
                    File.Copy(ApplicationPostDesInstallScriptPath, Path.Combine(RootDirectoryPathDi.FullName, New FileInfo(ApplicationPostDesInstallScriptPath).Name), True)
                End If
                scriptPostDesInstallPath = Path.Combine(AppInstallFullPath, New FileInfo(ApplicationPostDesInstallScriptPath).Name)
                BgwCreate.ReportProgress(68)
            End If

            Dim InstallerOK As Boolean
            Dim UninstallerOK As Boolean

            Dim src As String =
                "Imports System" & vbNewLine &
                "Imports System.IO" & vbNewLine &
                "Imports System.Reflection" & vbNewLine &
                "Imports Microsoft.Win32" & vbNewLine &
                "Imports System.Diagnostics" & vbNewLine &
                "Imports Microsoft.VisualBasic" & vbNewLine &
                "Namespace Package" & ApplicationInfos.AppGuid & vbNewLine &
                "Public Class Program" & vbNewLine &
                "   Public Shared Sub Main()" & vbNewLine &
                "       Dim AppStartup As String = My.Application.Info.DirectoryPath" & vbNewLine &
                "       If File.Exists(Path.Combine(AppStartup, " & Chr(34) & ResultFile & Chr(34) & "))" & vbNewLine &
                "           Dim RegistryPath as string = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "           Dim Is64BitsOS As Boolean = Environment.Is64BitOperatingSystem" & vbNewLine &
                "           Dim appArch as string = " & Chr(34) & ApplicationInfos.CpuTarget & Chr(34) & vbNewLine &
                "           If Is64BitsOS Then" & vbNewLine &
                "               If appArch = ""x86"" Then" & vbNewLine &
                "                   RegistryPath = ""SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "               Elseif appArch = ""x64"" Then" & vbNewLine &
                "                   RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "               Elseif appArch = ""x86 et x64"" Then" & vbNewLine &
                "                   RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "               End If" & vbNewLine &
                "           Else" & vbNewLine &
                "               RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "           End If" & vbNewLine &
                "           Dim InstallPath as string() = " & Chr(34) & AppInstallFullPath & Chr(34) & ".Split("":"")" & vbNewLine &
                "           Dim InstallRoot as string = InstallPath(0) & "":\""" & vbNewLine &
                "           Dim InstallDirectories As String() = InstallPath(1).Substring(1).Split(""\"")" & vbNewLine &
                "           Dim currentDir As String = InstallRoot" & vbNewLine &
                "           For i As Integer = 0 To InstallDirectories.Length - 1" & vbNewLine &
                "               currentDir = Path.Combine(currentDir, InstallDirectories(i))" & vbNewLine &
                "               If Not Directory.Exists(currentDir) Then" & vbNewLine &
                "                   Directory.CreateDirectory(currentDir)" & vbNewLine &
                "               End If" & vbNewLine &
                "           Next" & vbNewLine &
                "           If ProcessXcopy(AppStartup," & Chr(34) & AppInstallFullPath & Chr(34) & "," & Chr(34) & ExcludeCommandPath & Chr(34) & ") Then" & vbNewLine &
                "               Try" & vbNewLine &
                "                   Using RegKey As RegistryKey = Registry.LocalMachine" & vbNewLine &
                "                       Dim SubKey As RegistryKey = RegKey.OpenSubKey(RegistryPath, True)" & vbNewLine &
                "                       Dim AppKey As RegistryKey = SubKey.CreateSubKey(""" & ApplicationInfos.AppGuid & """, True)" & vbNewLine &
                "                       AppKey.SetValue(""DisplayIcon"", " & Chr(34) & IconPath & Chr(34) & ", RegistryValueKind.String)" & vbNewLine &
                "                       AppKey.SetValue(""DisplayName"", " & Chr(34) & ApplicationInfos.ProductName & Chr(34) & ", RegistryValueKind.String)" & vbNewLine &
                "                       AppKey.SetValue(""DisplayVersion"", " & Chr(34) & ApplicationInfos.FileVersion & Chr(34) & ", RegistryValueKind.String)" & vbNewLine &
                "                       AppKey.SetValue(""Publisher"", " & Chr(34) & ApplicationInfos.CompanyName & Chr(34) & ", RegistryValueKind.String)" & vbNewLine &
                "                       AppKey.SetValue(""NoModify"", 1, RegistryValueKind.DWord)" & vbNewLine &
                "                       AppKey.SetValue(""NoRepair"", 1, RegistryValueKind.DWord)" & vbNewLine &
                "                       AppKey.SetValue(""UninstallString"", " & Chr(34) & Path.Combine(AppInstallFullPath, ResultUninstFile) & Chr(34) & ", RegistryValueKind.String)" & vbNewLine &
                "                       AppKey.Close()" & vbNewLine &
                "                       SubKey.Close()" & vbNewLine &
                "                   End Using" & vbNewLine &
                "                 " & vbNewLine &
                If(ShortcutPath = "", "", "                   CreateShortcut(" & Chr(34) & Path.Combine(DestInstallPath, AppInstallFullPathFi.Name) & Chr(34) & "," & Chr(34) & ShortcutPath & Chr(34) & "," & Chr(34) & DestInstallPath & Chr(34) & "," & Chr(34) & If(IconPath.EndsWith(",0"), Path.Combine(DestInstallPath, AppInstallFullPathFi.Name), IconPath) & Chr(34) & ")") & vbNewLine &
                If(scriptPath = "", "", "        ApplicationExec(" & Chr(34) & scriptPath & Chr(34) & ")") & vbNewLine &
                "                   Environment.Exit(0)" & vbNewLine &
                "               Catch ex As exception " & vbNewLine &
                "                   Environment.Exit(-1)" & vbNewLine &
                "               End Try" & vbNewLine &
                "           Else" & vbNewLine &
                "               Environment.Exit(-1)" & vbNewLine &
                "           End If" & vbNewLine &
                "       Else" & vbNewLine &
                "           Environment.Exit(-1)" & vbNewLine &
                "       End If" & vbNewLine &
                "   End Sub" & vbNewLine &
                If(ShortcutPath = "", "", "   Private Shared Sub CreateShortCut(ProgramPath As String, ShortcutFullPath As String, WorkingDir As String, IconFullPath as string)" & vbNewLine &
                "       Dim objShell, objLink" & vbNewLine &
                "       objShell = CreateObject(""WScript.Shell"")" & vbNewLine &
                "       objLink = objShell.CreateShortcut(ShortcutFullPath)" & vbNewLine &
                "       objLink.TargetPath = ProgramPath " & vbNewLine &
                "       objLink.WorkingDirectory = WorkingDir " & vbNewLine &
                "       objLink.IconLocation = IconFullPath " & vbNewLine &
                "       objLink.Save" & vbNewLine &
                "   End Sub") & vbNewLine &
                "   Private Shared Sub CopyAll(source As DirectoryInfo,target As DirectoryInfo, ExcludedFilePath As String)" & vbNewLine &
                "       Directory.CreateDirectory(target.FullName)" & vbNewLine &
                "       For Each fi As FileInfo In source.GetFiles()" & vbNewLine &
                "           If Not fi.FullName = ExcludedFilePath Then" & vbNewLine &
                "               fi.CopyTo(Path.Combine(target.FullName, fi.Name), True)" & vbNewLine &
                "           End If" & vbNewLine &
                "       Next" & vbNewLine &
                "       For Each diSourceSubDir As DirectoryInfo In source.GetDirectories()" & vbNewLine &
                "           Dim nextTargetSubDir As DirectoryInfo = target.CreateSubdirectory(diSourceSubDir.Name)" & vbNewLine &
                "           CopyAll(diSourceSubDir, nextTargetSubDir, ExcludedFilePath)" & vbNewLine &
                "       Next" & vbNewLine &
                "   End Sub" & vbNewLine &
                "   Private Shared Function ProcessXcopy(SolutionDirectory As String, TargetDirectory As String, ExcludeInstallFileName As String) As Boolean" & vbNewLine &
                "       Dim diSource As DirectoryInfo = New DirectoryInfo(SolutionDirectory)" & vbNewLine &
                "       Dim diTarget As DirectoryInfo = New DirectoryInfo(TargetDirectory)" & vbNewLine &
                "       Dim fi as new fileinfo(Assembly.GetExecutingAssembly().Location)" & vbNewLine &
                "       Try" & vbNewLine &
                "           'CopyAll(diSource, diTarget, path.Combine(SolutionDirectory, " & Chr(34) & New FileInfo(CodedomDestFilePath).Name & Chr(34) & "))" & vbNewLine &
                "           CopyAll(diSource, diTarget, path.Combine(SolutionDirectory, fi.Name))" & vbNewLine &
                "           Return True" & vbNewLine &
                "       Catch ex as exception" & vbNewLine &
                "           Return false" & vbNewLine &
                "       End Try" & vbNewLine &
                "   End Function" & vbNewLine &
                If(scriptPath = "", "", "   Private Shared Sub ApplicationExec(appFullPath as string)" & vbNewLine &
                "       Try" & vbNewLine &
                "           Dim startInfo As ProcessStartInfo = New ProcessStartInfo(appFullPath)" & vbNewLine &
                "           startInfo.CreateNoWindow = true" & vbNewLine &
                "           startInfo.UseShellExecute = False" & vbNewLine &
                "           'startInfo.FileName = ""cmd.exe""" & vbNewLine &
                "           startInfo.WindowStyle = ProcessWindowStyle.Hidden" & vbNewLine &
                "           'Dim exeFileName2 As String = Chr(34) & appFullPath & Chr(34)" & vbNewLine &
                "           'startInfo.Arguments = ""/C "" & exeFileName2" & vbNewLine &
                "           Try" & vbNewLine &
                "               Using pc as process = Process.Start(startInfo)" & vbNewLine &
                "                   pc.WaitForExit()" & vbNewLine &
                "               End Using" & vbNewLine &
                "           Catch Ex as exception" & vbNewLine &
                "               'Msgbox(Ex.tostring)" & vbNewLine &
                "           End Try" & vbNewLine &
                "       Catch ex as exception" & vbNewLine &
                "           'Msgbox(ex.tostring)" & vbNewLine &
                "       End Try" & vbNewLine &
                "   End Sub") & vbNewLine &
                "End Class" & vbNewLine &
                "End Namespace" & vbNewLine

            If Helper.CreateStubFromString("Package" & ApplicationInfos.AppGuid & ".Program", src, InstallerIconPath, CodedomDestFilePath, ManifestPath, ApplicationInfos.CompanyName, ApplicationInfos.FileVersion, ApplicationInfos.ProductName) Then

                BgwCreate.ReportProgress(75)
                InstallerOK = True
                Dim srcUninst As String =
                "Imports System" & vbNewLine &
                "Imports System.IO" & vbNewLine &
                "Imports System.Reflection" & vbNewLine &
                "Imports Microsoft.Win32" & vbNewLine &
                "Imports System.Diagnostics" & vbNewLine &
                "Imports Microsoft.VisualBasic" & vbNewLine &
                "Namespace Package" & ApplicationInfos.AppGuid & vbNewLine &
                "Public Class Program" & vbNewLine &
                "   Private Shared UninstallProg as string" & vbNewLine &
                "   Public shared Sub Main()" & vbNewLine &
                "       Dim RegistryPath as string = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "       Dim Is64BitsOS As Boolean = Environment.Is64BitOperatingSystem" & vbNewLine &
                "       Dim appArch as string = " & Chr(34) & ApplicationInfos.CpuTarget & Chr(34) & vbNewLine &
                "       If Is64BitsOS Then" & vbNewLine &
                "           If appArch = ""x86"" Then" & vbNewLine &
                "               RegistryPath = ""SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "           Elseif appArch = ""x64"" Then" & vbNewLine &
                "               RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "           Elseif appArch = ""x86 et x64"" Then" & vbNewLine &
                "               RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "           End If" & vbNewLine &
                "       Else" & vbNewLine &
                "           RegistryPath = ""SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall""" & vbNewLine &
                "       End If" & vbNewLine &
                "       Try" & vbNewLine &
                "           Using RegKey As RegistryKey = Registry.LocalMachine" & vbNewLine &
                "               Dim SubKey As RegistryKey = RegKey.OpenSubKey(RegistryPath & ""\"" & """ & ApplicationInfos.AppGuid & """)" & vbNewLine &
                "               If Not SubKey Is Nothing Then" & vbNewLine &
                "                   UninstallProg = SubKey.GetValue(""UninstallString"")" & vbNewLine &
                "                   If File.Exists(UninstallProg) Then" & vbNewLine &
                "                       UninstallProg = New FileInfo(UninstallProg).DirectoryName" & vbNewLine &
                "                       For Each Proc As Process In Process.GetProcessesByName(" & Chr(34) & ProgFileName & Chr(34) & ")" & vbNewLine &
                "                           Try" & vbNewLine &
                "                               Proc.kill" & vbNewLine &
                "                               proc.WaitForExit()" & vbNewLine &
                "                           Catch Ex As Exception" & vbNewLine &
                "                           End Try" & vbNewLine &
                "                       Next" & vbNewLine &
                "                       Try" & vbNewLine &
                "                           Using RegKey2 As RegistryKey = Registry.LocalMachine" & vbNewLine &
                "                               Dim SubKey2 As RegistryKey = RegKey2.OpenSubKey(RegistryPath, True)" & vbNewLine &
                "                               If Not SubKey2 Is Nothing Then" & vbNewLine &
                "                                   SubKey2.DeleteSubKeyTree(""" & ApplicationInfos.AppGuid & """)" & vbNewLine &
                "                                   SubKey2.Close()" & vbNewLine &
                "                               End If" & vbNewLine &
                "                           End Using" & vbNewLine &
                "                       Catch Ex As Exception " & vbNewLine &
                "                           'Msgbox(ex.tostring)" & vbNewLine &
                "                       End try" & vbNewLine &
                "                       SubKey.Close()" & vbNewLine &
                "                       Dim fi as new fileinfo(Assembly.GetEntryAssembly().Location)" & vbNewLine &
                "                       if fi.DirectoryName.tolower = UninstallProg.tolower Then" & vbNewLine &
                "                           AddHandler AppDomain.CurrentDomain.ProcessExit, New EventHandler(AddressOf CurrentDomain_ProcessExit)" & vbNewLine &
                "                       End if" & vbNewLine &
                "                       ProcessSelfDeletion(UninstallProg)" & vbNewLine &
                "                       Environment.Exit(0)" & vbNewLine &
                "                   Else" & vbNewLine &
                "                       UninstallProg = """"" & vbNewLine &
                "                   End If" & vbNewLine &
                "                   SubKey.Close()" & vbNewLine &
                "                   Environment.Exit(0)" & vbNewLine &
                "               End If" & vbNewLine &
                "           End Using" & vbNewLine &
                "       Catch ex as exception " & vbNewLine &
                "           'Msgbox(ex.tostring)" & vbNewLine &
                "       End Try" & vbNewLine &
                "   End Sub" & vbNewLine &
                "   Private Shared Sub CurrentDomain_ProcessExit(sender As Object, e As EventArgs)" & vbNewLine &
                "       Try" & vbNewLine &
                "           Dim fi as new fileinfo(Assembly.GetEntryAssembly().Location)" & vbNewLine &
                "           Dim startInfo As ProcessStartInfo = New ProcessStartInfo()" & vbNewLine &
                "           startInfo.CreateNoWindow = true" & vbNewLine &
                "           startInfo.UseShellExecute = False" & vbNewLine &
                "           startInfo.FileName = ""cmd.exe""" & vbNewLine &
                "           startInfo.WindowStyle = ProcessWindowStyle.Hidden" & vbNewLine &
                "           Dim exeFileName2 As String = Chr(34) & Path.Combine(""C:\Windows\Temp\"", fi.name) & Chr(34)" & vbNewLine &
                "           startInfo.Arguments = ""/C ping 1.1.1.1 -n 1 -w 5000 > Nul & Del /F /Q /S "" & exeFileName2" & vbNewLine &
                "           Try" & vbNewLine &
                "               Process.Start(startInfo)" & vbNewLine &
                "           Catch Ex as exception" & vbNewLine &
                "               'Msgbox(Ex.tostring)" & vbNewLine &
                "           End Try" & vbNewLine &
                "       Catch ex as exception" & vbNewLine &
                "           'Msgbox(ex.tostring)" & vbNewLine &
                "       End Try" & vbNewLine &
                "   End Sub" & vbNewLine &
                "   Private Shared Sub ProcessSelfDeletion(exeFileName as string)" & vbNewLine &
                "       If Directory.Exists(exeFileName) Then" & vbNewLine &
                If(scriptPostDesInstallPath = "", "", "        ApplicationExec(" & Chr(34) & scriptPostDesInstallPath & Chr(34) & ")") & vbNewLine &
                "           Dim fi as new fileinfo(Assembly.GetEntryAssembly().Location)" & vbNewLine &
                "           if fi.DirectoryName.tolower = UninstallProg.tolower Then" & vbNewLine &
                "               File.move(fi.fullname, Path.Combine(""C:\Windows\Temp\"", fi.name))" & vbNewLine &
                "           End if" & vbNewLine &
                "           Dim startInfo As ProcessStartInfo = New ProcessStartInfo()" & vbNewLine &
                "           startInfo.CreateNoWindow = true" & vbNewLine &
                "           startInfo.UseShellExecute = False" & vbNewLine &
                "           startInfo.FileName = ""cmd.exe""" & vbNewLine &
                "           startInfo.WindowStyle = ProcessWindowStyle.Hidden" & vbNewLine &
                "           Dim exeFileName2 As String = Chr(34) & exeFileName & Chr(34)" & vbNewLine &
                "           Dim ShortCutPath as string = " & Chr(34) & ShortcutPath & Chr(34) & vbNewLine &
                If(ShortcutPath = "", "", "           If File.Exists(ShortCutPath) Then" & vbNewLine &
                "               File.Delete(ShortCutPath)" & vbNewLine &
                "           End If") & vbNewLine &
                "           startInfo.Arguments = ""/C ping 127.0.0.1 -n 1 > NUL & RMDIR /Q /S "" & exeFileName2" & vbNewLine &
                "           Try" & vbNewLine &
                "               Using pc as process = Process.Start(startInfo)" & vbNewLine &
                "                   pc.WaitForExit()" & vbNewLine &
                "               End Using" & vbNewLine &
                "           Catch Ex as exception" & vbNewLine &
                "               'Msgbox(Ex.tostring)" & vbNewLine &
                "           End Try" & vbNewLine &
                "       End If" & vbNewLine &
                "   End Sub" & vbNewLine &
                If(scriptPostDesInstallPath = "", "", "   Private Shared Sub ApplicationExec(appFullPath as string)" & vbNewLine &
                "       Try" & vbNewLine &
                "           Dim startInfo As ProcessStartInfo = New ProcessStartInfo(appFullPath)" & vbNewLine &
                "           startInfo.CreateNoWindow = true" & vbNewLine &
                "           startInfo.UseShellExecute = False" & vbNewLine &
                "           'startInfo.FileName = ""cmd.exe""" & vbNewLine &
                "           startInfo.WindowStyle = ProcessWindowStyle.Hidden" & vbNewLine &
                "           'Dim exeFileName2 As String = Chr(34) & appFullPath & Chr(34)" & vbNewLine &
                "           'startInfo.Arguments = ""/C "" & exeFileName2" & vbNewLine &
                "           Try" & vbNewLine &
                "               Using pc as process = Process.Start(startInfo)" & vbNewLine &
                "                   pc.WaitForExit()" & vbNewLine &
                "               End Using" & vbNewLine &
                "           Catch Ex as exception" & vbNewLine &
                "               'Msgbox(Ex.tostring)" & vbNewLine &
                "           End Try" & vbNewLine &
                "       Catch ex as exception" & vbNewLine &
                "           'Msgbox(ex.tostring)" & vbNewLine &
                "       End Try" & vbNewLine &
                "   End Sub") & vbNewLine &
                "End Class" & vbNewLine &
                "End Namespace" & vbNewLine

                If Helper.CreateStubFromString("Package" & ApplicationInfos.AppGuid & ".Program", srcUninst, UninstallerIconPath, CodedomDestFilePathUninst, ManifestPath, ApplicationInfos.CompanyName, ApplicationInfos.FileVersion, ApplicationInfos.ProductName) Then
                    UninstallerOK = True

                    BgwCreate.ReportProgress(95)
                Else
                    If File.Exists(CodedomDestFilePathUninst) Then
                        File.Delete(CodedomDestFilePathUninst)
                    End If
                End If
            Else
                If File.Exists(CodedomDestFilePath) Then
                    File.Delete(CodedomDestFilePath)
                End If
            End If

            If File.Exists(ManifestPath) Then
                File.Delete(ManifestPath)
            End If

            If File.Exists(InstallerIconPath) Then
                File.Delete(InstallerIconPath)
            End If

            If File.Exists(UninstallerIconPath) Then
                File.Delete(UninstallerIconPath)
            End If

            BgwCreate.ReportProgress(100)

            If InstallerOK = True AndAlso UninstallerOK = True Then
                e.Result = Messages.BinariesCreated & RootDirectoryPathDi.FullName & "|" & AppInstallFullPath & "|" & ResultUninstFile
            ElseIf InstallerOK = False AndAlso UninstallerOK = True Then
                e.Result = Messages.BinaryInstallerError
            ElseIf InstallerOK = True AndAlso UninstallerOK = False Then
                e.Result = Messages.BinaryUninstallerError
            ElseIf InstallerOK = False AndAlso UninstallerOK = False Then
                e.Result = Messages.BinariesNotCreated
            End If

        Catch ex As Exception
            e.Result = "ERREUR : " & ex.ToString
        End Try
    End Sub

    'Private Shared Sub Copy(sourceDirectory As String, targetDirectory As String, ExcludedFilePath As String)
    '    Dim diSource As DirectoryInfo = New DirectoryInfo(sourceDirectory)
    '    Dim diTarget As DirectoryInfo = New DirectoryInfo(targetDirectory)
    '    CopyAll(diSource, diTarget, ExcludedFilePath)
    'End Sub

    'Private Shared Sub CopyAll(ByVal source As DirectoryInfo, ByVal target As DirectoryInfo, ExcludedFilePath As String)
    '    Directory.CreateDirectory(target.FullName)

    '    For Each fi As FileInfo In source.GetFiles()
    '        If Not fi.FullName = ExcludedFilePath Then
    '            fi.CopyTo(Path.Combine(target.FullName, fi.Name), True)
    '        End If
    '    Next

    '    For Each diSourceSubDir As DirectoryInfo In source.GetDirectories()
    '        Dim nextTargetSubDir As DirectoryInfo = target.CreateSubdirectory(diSourceSubDir.Name)
    '        CopyAll(diSourceSubDir, nextTargetSubDir, ExcludedFilePath)
    '    Next
    'End Sub


    Private Sub BgwCreate_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)
        RaiseEvent CreationPackageProgressChanged(sender, e)
    End Sub

    Private Sub BgwCreate_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        RaiseEvent CreationPackageRunWorkerCompleted(sender, e)
    End Sub







#End Region

End Class
