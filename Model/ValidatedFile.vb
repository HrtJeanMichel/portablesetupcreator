﻿Public NotInheritable Class ValidatedFile
    Inherits EventArgs

#Region " Properties "
    Public ReadOnly Property IsValid As Boolean
    Public ReadOnly Property Infos As AppInfos
    Public ReadOnly Property Message As String
#End Region

#Region " Constructor "
    Public Sub New(isvalid As Boolean, peInfos As AppInfos, Mess As String)
        _IsValid = isvalid
        _Infos = peInfos
        _Message = Mess
    End Sub
#End Region

End Class
