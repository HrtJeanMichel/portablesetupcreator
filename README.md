# **Portable SETUP Creator** #

# Description

Créer un installeur et un désinstalleur pour les applications portables. 
Il est possible d'embarquer la création d'un raccourci bureau ou menu démarrer dans l'installeur ainsi que l'exécution d'un script post-installation. 
Cet outil facilite l'implémentation des chaînes d'installation et de désinstallation dans SCCM puisqu'il permet de s'affranchir de la création de fichiers de commande (.bat, .cmd, etc...) pour installer ou désinstaller une application portable.


# Screenshot

![PortableSetupCreator0.png](http://i.imgur.com/Lye9pTq.png)


# Features

* Prise en charge des fichiers portants l'extension .EXE, .HTML, .HTM
* Affiche les informations du binaire sélectionné (VERSION_INFOS) : Nom, version, éditeur, cible CPU, type dotnet ou Native
* Affiche l'icône du binaire sélectionné
* Sélection d'un fichier .ICO (pratique lorsqu'on sélectionne un fichier .HTM ou .HTML et utile pour obtenir une icône personnalisée de raccourci)
* Choix de l'emplacement d'installation ou définition par défaut selon le CPU cible et l'architecture OS sélectionnée
* Sélection du type de raccourci : aucun, bureau ou menu démarré
* Possibilité d'ajouter un script .BAT ou .CMD qui sera exécuté avant l'installation de l'application
* Possibilité d'ajouter un script .BAT ou .CMD qui sera exécuté après l'installation de l'application
* Génère deux binaires : Uninstall-<GUID>.exe, Install-<GUID>.exe

# Prerequisites

* Windows 10, Server 2016 +
* DotNet Framework 4.5
* Application portable


# WebSite


# Credits


# Copyright

Copyright © Hrtjm 2008-2022


# Licence
