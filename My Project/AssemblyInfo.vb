﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Vérifiez les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("PortableSetupCreator")>
<Assembly: AssemblyDescription("Cet utilitaire portable développé en langage de programmation DotNet génère un installeur et un désinstalleur pour les applications portables. Il est possible d'embarquer la création d'un raccourci bureau ou menu démarrer dans l'installeur ainsi que l'exécution d'un script post-installation. Cet outil facilite l'implémentation des chaînes d'installation et de désinstallation dans SCCM puisqu'il permet de s'affranchir de la création de fichiers de commandes (.bat, .cmd, etc...) pour installer ou désinstaller une application portable.")>
<Assembly: AssemblyCompany("Hrtjm")>
<Assembly: AssemblyProduct("PortableSetupCreator")>
<Assembly: AssemblyCopyright("Copyright © Hrtjm 2022")>
<Assembly: AssemblyTrademark("Copyright © Hrtjm 2022")>

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("5259edb7-a8d9-48b1-b7b1-be559524040d")>

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("2.0.1.2")>
<Assembly: AssemblyFileVersion("2.0.1.2")>
